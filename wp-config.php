<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'quadpay' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '!{r-=u(];3T!$R@#x9d@Me2G;6{F]R{Q,pCGunq?~/P}kx(_3f::E=qtN1=UpDR*' );
define( 'SECURE_AUTH_KEY',  '`1pdzU?j,K ]h?k]_wz0;.; &UeK{BEsPkB1D[nQAkDHHg~)R_TKs0U Ep:Rd`>}' );
define( 'LOGGED_IN_KEY',    '|2elFJ,L9`Fz`cE]I1$e?X+#[K*PUD,5@o}MpEk3PW?4BQi/zL~ky/R3)mF8Aw9a' );
define( 'NONCE_KEY',        '|,HB;Pd7<23ht@Y|8Aw%{tp3q$.QG5,0%ikVjblTn@U52-kG)+0z*oA<2DZaD~jl' );
define( 'AUTH_SALT',        'cw(eYyIpJY=V:_v{q9s.CH2:/$^S>&g(/$C[XJDf1H!FwttSp/~9;neC7Jl?VNDQ' );
define( 'SECURE_AUTH_SALT', '~mVLWEgM|`WnwmuURM}>n(nEOq`L~8><}uJyUhEOS.#Z0,9UV28zTH 7BU3,7g1j' );
define( 'LOGGED_IN_SALT',   'g[yM:uqqg9EA:cZh>W6aw0am|WX{HL[l:2c{Ti c8*zfjj>:T)_-$&@*5ZlS+[Sn' );
define( 'NONCE_SALT',       'KyFl4***3SisiiyH)$Zekyt4oWD&k:}U0XT<a@e}r044?)xIuu2.09IAkK&HVMJG' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );
define( 'ALLOW_UNFILTERED_UPLOADS', true );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
