<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="apple-itunes-app" content="app-id=1425045070">
<link rel="profile" href="http://gmpg.org/xfn/11">

<!-- Favicon -->
<?php
$favicon = get_field('favicon', 'option');
if ($favicon) {
    ?>
            <link rel="shortcut icon" type="image/x-icon" href="<?php echo $favicon['url']; ?>">
            <?php } ?>
                <!-- Favicon end -->
<?php wp_head(); ?>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-P5Z6DFW');</script>
<!-- End Google Tag Manager -->

</head>

<body <?php body_class(); ?>>

	<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P5Z6DFW"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<div class="wrapper">

	<section class="anystore_split_Sec">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 text-center">
					<?php echo get_field('top_content','options');?>


		
					<div class="store_link">

					<?php while( have_rows('add_links','options') ): the_row(); 

// vars
$image = get_sub_field('logo','options');
$link = get_sub_field('link','options');

?>
<a target="_blank" href="<?php echo $link;?>">
						<?php if($image) { ?>
						<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />
</a>
						<?php } ?>

						
						<?php endwhile;?>
						
					</div>

				</div>
			</div>
		</div>
	</section>
	<!-- Header Starts Here -->
	<header id="header">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-5 col-lg-4 col-xl-4">
				<?php
					wp_nav_menu(
						array(
							'theme_location' => 'mobile-menu',
							'menu_class'        => 'mobile_top_menu',
						)
					);
				?>
				<?php $header_icon = get_field('logo', 'options');
						if ($header_icon) {?>
						<a class="logo" href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo $header_icon['title']; ?>">
						<img src="<?php echo $header_icon['url']; ?>" alt="<?php echo $header_icon['alt']; ?>"/>
						</a>
				<?php }?>
				<?php $help_link = get_field('help_link','options');?>
				<?php if($help_link){ ?> <a class="help_link" href="<?php echo $help_link['url']; ?>" <?php if($help_link[ 'target']) { ?>target="_blank" <?php } ?> title="<?php echo $help_link['title']; ?>"><?php echo $help_link['title']; ?></a>
												<?php } ?>
				</div>
				<div class="col-sm-12 col-md-7 col-lg-8 col-xl-8">
					<?php get_template_part( 'template-parts/navigation/navigation', 'top' ); ?>
				</div>
			</div>
		</div>
	</header>   

	<div class="mob-banner-sec">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 col-md-6 col-sm-6">
					<h2><?php echo get_field('mobile_app_text','options');?></h2>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6">
				
				<?php $mobile_app_image = get_field('mobile_app_image','options');?>
				<?php if($mobile_app_image){ ?>
					<div class="mob-banner-img">
					<img src="<?php echo $mobile_app_image['url']; ?>" alt="<?php echo $mobile_app_image['alt']; ?>"/>
					</div>
				<?php } ?>
				</div>
			</div>
		</div>
	</div>

	<!-- Header Ends Here -->
