<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>
<div class="blog-block clearfix">
   
   <?php $image = wp_get_attachment_url(get_post_thumbnail_id($post->ID, 'full')); ?>
				  
				  
					  <div class="news-block-img" <?php if(!empty($image)){ ?> style="background-image: url(<?php echo $image; ?>);" <?php } ?>>
					  </div>
	 <div class="blog-block-content">
	  <h3>
			  <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title();?></a>
		  </h3>
		  <p><?php echo substr(strip_tags(get_the_excerpt()), 0,250)."...";?></p>
							  <?php //echo substr(get_the_excerpt(), 0,300)."…";?>
		  <div class="read-more">
			  <a href="<?php the_permalink(); ?>" class="button" title="Read More">Read More</a>
		  </div>
	 </div>
  </div>
  