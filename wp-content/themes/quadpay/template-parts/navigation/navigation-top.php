<?php
/**
 * Displays top navigation
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>
<nav class="navbar navbar-expand-md navbar-light">
	<button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    	<span class="navbar-toggler-icon"></span>
	</button>
	<div class="collapse navbar-collapse justify-content-end" id="navbarSupportedContent">
		<?php
		wp_nav_menu(
			array(
				'theme_location' => 'top',
				'menu_id'        => 'top-menu',
			)
		);
		?>
		<div class="anystore_split_Sec">
			<?php echo get_field('top_content','options');?>
			<div class="store_link">
			<?php while( have_rows('add_links','options') ): the_row(); 
			// vars
			$image = get_sub_field('logo','options');
			$link = get_sub_field('link','options');

			?>
			<a target="_blank" href="<?php echo $link;?>">
				<?php if($image) { ?>
				<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />
			</a>
				<?php } ?>
				<?php endwhile;?>
			</div>
		</div>
	</div>
</nav>