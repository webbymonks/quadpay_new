<?php
/*
* Template Name: Get Started
*/

get_header(); ?>


<section class="comman_sec account_type_Sec">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 text-center">
                <h2>Start using QuadPay Today</h2>
                <p>Please select the account type you would like to create an account as</p>
                <div class="row justify-content-md-center">
                    <div class="col-sm-12 col-md-9 col-lg-9">
                        <div class="row">
                            <div class="col-sm-12 col-md-6 col-lg-6">
                                <div class="account_type_content">
                                    <figure style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/images/customer_img.jpg');"><!-- Image Here --></figure>
                                    <a href="#signupCustomer" title="Sign up as a customer" class="button" data-toggle="modal" data-target="#signupCustomer">SIGN UP AS A CUSTOMER</a>
                                    <!-- Modal -->
                                    <div class="modal fade" id="signupCustomer" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                    <h3>SIGN UP AS A CUSTOMER</h3>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="sign_up_forms">
                                                        <?php echo do_shortcode('[gravityform id=5 title=false description=false ajax=true tabindex=49]') ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-6">
                                <div class="account_type_content">
                                    <figure style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/images/business_img.jpg');"><!-- Image Here --></figure>
                                    <a href="#signupBusiness" title="Sign up as a Business" class="button border_btn" data-toggle="modal" data-target="#signupBusiness">SIGN UP AS A BUSINESS</a>
                                    <!-- Modal -->
                                    <div class="modal fade" id="signupBusiness" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                    <h3>SIGN UP AS A BUSINESS</h3>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="sign_up_forms">
                                                        <?php echo do_shortcode('[gravityform id=6 title=false description=false ajax=true tabindex=49]') ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>