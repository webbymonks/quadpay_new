<?php
/*
* Template Name: App
*/

get_header(); ?>

	
<!-- Banner Section Starts Here -->
<section class="banner_sec">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-7 col-lg-7 col-xl-7">
                <div class="banner_content">
                    <h1>Buy anything, anywhere in 4 interest-free payments</h1>
                    <p>Use QuadPay to pay online or in-store</p>
                    <?php echo do_shortcode('[gravityform id=1 title=false description=false ajax=true tabindex=49]') ?>
                    <ul>
                        <li><a href="#" title="Get on the App Store"><i class="fab fa-apple"></i> Get on the App Store</a></li>
                        <li><a href="#" title="Google Play coming soon"><i class="fab fa-google-play"></i> Google Play coming soon</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-12 col-md-5 col-lg-5 col-xl-5">
                <figure class="banner_img">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/banner_img.png" alt="" />
                </figure>
            </div>
        </div>
    </div>
</section>
<!-- Banner Section Ends Here -->

<!-- Unlimited Possibiliteis Section Starts Here -->
<section class="unlimited_possibiliteis_sec">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 text-center">
                <div class="heading_content">
                    <h4>Unlimited Possibilities</h4>
                    <h2>Where can you use the QuadPay app? Anywhere.</h2>
                    <p>Use QuadPay anywhere where Visa is accepted</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-3">
                <a href="#" title="NORDSTROM" class="unli_possi_img" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/images/nordstrom.png');">
                    <figure>
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/nordstrom_logo.png" alt="" />
                    </figure>
                </a>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-3">
                <a href="#" title="UGG" class="unli_possi_img" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/images/ugg.png');">
                    <figure>
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/" alt="" />
                    </figure>
                </a>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-3">
                <a href="#" title="FASHIONNOVa" class="unli_possi_img" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/images/fashionnova.png');">
                    <figure>
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/fashionnova_logo.png" alt="" />
                    </figure>
                </a>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-3">
                <a href="#" title="ZARA" class="unli_possi_img" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/images/zara.png');">
                    <figure>
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/zara_logo.png" alt="" />
                    </figure>
                </a>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-3">
                <a href="#" title="VICTORIA SECRET" class="unli_possi_img" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/images/victoria_secret.png');">
                    <figure>
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/" alt="" />
                    </figure>
                </a>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-3">
                <a href="#" title="NIKE" class="unli_possi_img" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/images/nike.png');">
                    <figure>
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/nike_logo.png" alt="" />
                    </figure>
                </a>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-3">
                <a href="#" title="WHOLE FOOD MARKETS" class="unli_possi_img" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/images/whole_food_markets.png');">
                    <figure>
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/" alt="" />
                    </figure>
                </a>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-3">
                <a href="#" title="H&M" class="unli_possi_img" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/images/h-m.png');">
                    <figure>
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/h-m-Logo.png" alt="" />
                    </figure>
                </a>
            </div>
        </div>
    </div>
</section>
<!-- Unlimited Possibiliteis Section Ends Here -->

<!-- Simple Steps Section Starts Here -->
<section class="simple_steps_sec">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 text-center">
				<div class="heading_content">
					<h2>Buy from anywhere in 3 simple steps</h2>
					<p>Install the app, link your debit or credit card and start shopping. It’s that easy.</p>
				</div>
				<div class="tabing_wrap clearfix">
					<div class="tabs_content">
						<h4>Buy Online and In-store</h4>
						<h2>How it works</h2>
						<div class="nav nav-tabs" id="nav-tab" role="tablist">
							<div class="nav-item active" id="store-website-tab" data-toggle="tab" href="#store-website" role="tab" aria-controls="store-website" aria-selected="true">
								<h3>Search for a store or website</h3>
								<p>Look up the store you would like to transact at or enter a website address.</p>
							</div>
							<div class="nav-item" id="purchase-amount-tab" data-toggle="tab" href="#purchase-amount" role="tab" aria-controls="purchase-amount" aria-selected="false">
								<h3>Enter your purchase amount</h3>
								<p>Enter the amount of your purchase including taxes and shipping.</p>
							</div>
							<div class="nav-item" id="pay-anywhere-tab" data-toggle="tab" href="#pay-anywhere" role="tab" aria-controls="pay-anywhere" aria-selected="false">
								<h3>Swipe, dip or tap to pay anywhere</h3>
								<p>Swipe, dip or tap your QuadPay Visa Card at any store, or use it online to complete your purchase.</p>
							</div>
						</div>
					</div>
					<div class="tab-content" id="nav-tabContent">
						<div class="tab-pane fade show active" id="store-website" role="tabpanel" aria-labelledby="store-website-tab">
							<figure>
								<img src="<?php echo get_template_directory_uri(); ?>/assets/images/phone_img.png" alt="" />
							</figure>
						</div>
						<div class="tab-pane fade" id="purchase-amount" role="tabpanel" aria-labelledby="purchase-amount-tab">
							<figure>
								<img src="<?php echo get_template_directory_uri(); ?>/assets/images/phone_img.png" alt="" />
							</figure>
						</div>
						<div class="tab-pane fade" id="pay-anywhere" role="tabpanel" aria-labelledby="pay-anywhere-tab">
							<figure>
								<img src="<?php echo get_template_directory_uri(); ?>/assets/images/phone_img.png" alt="" />
							</figure>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Simple Steps Section Ends Here -->

<!-- Content Image Section Starts Here -->
<section class="middle_content_img_sec">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-sm-12 col-md-5 col-lg-5 col-xl-5 order-sm-1 order-md-2 order-lg-2 order-xl-2">
                <figure class="content_img">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/phone.png" alt="" />
                </figure>
            </div>
            <div class="col-sm-12 col-md-7 col-lg-7 col-xl-7 order-sm-2 order-md-1 order-lg-1 order-xl-1">
                <div class="middle_content">
                    <h2>Pay with QuadPay on any website</h2>
                    <p>Enter the website address of any website to start shopping with QuadPay in the app.</p>
                    <p>Use the QuadPay Visa Card number displayed in the app when you check out and your order will be split in 4.</p>
                    <a href="#" title="Explore Our Directory" class="button">Explore Our Directory</a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Content Image Section Ends Here -->

<!-- Frequently Asked Question Section Starts Here -->
<section class="comman_sec frequently_question_sec">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12">
                <h3>Frequently Asked Questions</h3>
                <div id="accordion">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <div class="collapsed" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                            Can I use QuadPay on sites that don’t accept QuadPay?
                            </div>
                        </div>
                        <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <div class="collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                            Are there any fees to use QuadPay? 
                            </div>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingThree">
                            <div class="collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                What information do you collect?
                            </div>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                            <div class="card-body">
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Frequently Asked Question Section Ends Here -->

<?php get_footer(); ?>