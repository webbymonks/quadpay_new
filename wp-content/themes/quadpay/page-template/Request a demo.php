<?php
/*
* Template Name: Request a demo
*/

get_header(); ?>
	<!--form Section Starts Here -->
	<section class="comman_sec mid_content_sec form_request_section" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/images/form-bg.png)">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-6 col-lg-6 col-xl-6 order-sm-1 order-md-2 order-lg-2 order-xl-2">
					<div class="form_wrap">
						<div class="sign_up_forms">
							<?php echo do_shortcode('[gravityform id=4 title=false description=false ajax=true tabindex=49]') ?>
						</div>
					</div>
				</div>
				<div class="col-sm-12 col-md-6 col-lg-6 col-xl-6 order-sm-1 order-md-2 order-lg-2 order-xl-2">
					<div class="form-detail">
						<h2>Schedule your personal  demo</h2>
						<p>Just andswer a few simple questions so we can personalize the right experience for you.</p>
						<ul class="listing">
							<li><strong>Conversion rate and Topline sales</strong> up more than 20%</li>
							<li>Increased <strong>repeat customer rate</strong> up to 80%</li>
							<li>Up to 20% of <strong>all sales processed through QuadPay</strong></li>
							<li><strong>Average basket size</strong> up more than 20% and as high as 60%</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- form Section end Here -->
	<!-- Client Logos Section Starts Here -->
	<section class="comman_sec clinet_logo_sec">
		<div class="container">
			<p><strong>Quick and easy integration managed by our team, OR BY YOURS</strong></p>
			<div class="row align-items-start">
				<div class="col-6 col-md-3">
					<figure> <img src="<?php echo get_template_directory_uri(); ?>/assets/images/client_logo_1.png" alt="client logo" /> </figure>
				</div>
				<div class="col-6 col-md-3">
					<figure> <img src="<?php echo get_template_directory_uri(); ?>/assets/images/client_logo_2.png" alt="client logo" /> </figure>
				</div>
				<div class="col-6 col-md-3">
					<figure> <img src="<?php echo get_template_directory_uri(); ?>/assets/images/client_logo_3.png" alt="client logo" /> </figure>
				</div>
				<div class="col-6 col-md-3">
					<figure> <img src="<?php echo get_template_directory_uri(); ?>/assets/images/client_logo_4.png" alt="client logo" /> </figure>
				</div>
			</div>
			<div class="row justify-content-center">
				<div class="col-3">
					<figure> <img src="<?php echo get_template_directory_uri(); ?>/assets/images/client_logo_5.png" alt="client logo" /> </figure>
				</div>
				<div class="col-3">
					<figure> <img src="<?php echo get_template_directory_uri(); ?>/assets/images/client_logo_6.png" alt="client logo" /> </figure>
				</div>
			</div>
		</div>
	</section>
	<!-- Client Logos Section Ends Here -->
	<?php get_footer(); ?>