<?php
/*
* Template Name: Privacy
*/

get_header(); ?>
	<!-- privacy Section Starts Here -->
	<section class="comman_sec privacy_sec">
		<div class="container">
		    <h1>Privacy Policy</h1>
			<h3>Your Privacy Rights</h3>
			<p>This Privacy Policy describes your privacy rights regarding our collection, use, storage, sharing and protection of your personal information. It applies to the QuadPay website and all related sites, applications, services and tools regardless of how you access or use them.</p>
			<h3>Scope and Consent</h3>
			<p>You accept this Privacy Policy when you sign up for, access, or use our products, services, content, features, technologies or functions offered on our website and all related sites, applications, and services (collectively “QuadPay Services”). This Privacy Policy applies US law and is intended to govern the use of QuadPay Services by our users (including, without limitation those who use the QuadPay Services in the course of their trade or business) in the United States, unless otherwise agreed through contract. We may amend this Privacy Policy at any time by posting a revised version on our website. The revised version will be effective as of the published effective date. In addition, if the revised version includes a substantial change, we will provide you with 30 days’ prior notice by posting notice of the change on the “Policy Updates” page of our website. After this 30-day notice period, you will be considered as having expressly consented to all amendments to this Privacy Policy. </p>
			<h3>Collection of Personal Information</h3>
			<p> We collect the following types of personal information in order to provide you with the use of QuadPay Services, and to help us personalize and improve your experience. </p>
			<p>Information we collect automatically: When you use QuadPay Services, we collect information sent to us by your computer, mobile phone or other access device. The information sent to us includes, but is not limited to, the following: data about the pages you access, computer IP address, device ID or unique identifier, device type, geo-location information, computer and connection information, mobile network information, statistics on page views, traffic to and from the sites, referral URL, ad data, and standard web log data and other information. We also collect anonymous information through our use of cookies and web beacons. </p>
			<p>Information you provide to us: We may collect and store any information you provide us when you use QuadPay Services, including when you add information on a web form, add or update your account information, participate in community discussions, chats, or dispute resolutions, or when you otherwise correspond with us regarding QuadPay Services.</p>
			<p>When you use QuadPay Services, we also collect information about your transactions and your activities. In addition, if you open a QuadPay account or use QuadPay Services, we may collect the following types of information:</p>
		</div>
	</section>
	<!-- privacy Section Starts Here -->
	<?php get_footer(); ?>