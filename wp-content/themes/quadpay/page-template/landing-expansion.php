<?php
/*
* Template Name: Landing Expansion
*/

get_header(); ?>


<section class="comman_sec mid_content_sec expansion_sec">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 text-center">
                <div class="mid_content">
                    <p><img src="<?php echo get_template_directory_uri(); ?>/assets/images/expansion_img.jpg" alt="" /></p>
                    <h2>QuadPay is going transatlantic</h2>
                    <p>We are expanding quickly and we want the UK to be one of the first international partners of QuadPay.</p>
                    <a href="#signupmerchant" title="Sign up as merchant" class="button" data-toggle="modal" data-target="#signupmerchant">SIGN UP AS MERCHANT</a>
                    <a href="#joinWaitingList" title="Join customer waiting list" class="button" data-toggle="modal" data-target="#joinWaitingList">JOIN CUSTOMER WAITING LIST</a>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Modal -->
<div class="modal fade" id="signupmerchant" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h3>Sign up as merchant</h3>
            </div>
            <div class="modal-body">
                <div class="sign_up_forms">
                    <?php echo do_shortcode('[gravityform id=2 title=false description=false ajax=true tabindex=49]') ?>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="joinWaitingList" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h3>Join customer waiting list</h3>
            </div>
            <div class="modal-body">
                <div class="sign_up_forms">
                    <?php echo do_shortcode('[gravityform id=3 title=false description=false ajax=true tabindex=49]') ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>