<?php
/*
* Template Name: About
*/

get_header(); ?>

<!-- Mid Content Section Starts Here -->
<section class="comman_sec mid_content_sec">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 text-center">
                <div class="mid_content">
                    <h2>What you see is what you pay</h2>
                    <p>QuadPay is revolutionizing the way people shop and transact both online and in-store.</p>
                    <figure>
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/group_img.jpg" alt="" />
                    </figure>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Mid Content Section Starts Here -->

<!-- Mid Content Section Starts Here -->
<section class="comman_sec mid_content_sec">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 text-center">
                <div class="mid_content">
                    <h2>The payment platform for everyone</h2>
                    <p>QuadPay is a consumer centric payment platform that exists to transform the way shoppers pay for their purchases by offering simple and transparent payment methods.</p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Mid Content Section Starts Here -->

<!-- Our Value Section Starts Here -->
<section class="comman_sec our_value_sec">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 text-center">
                <h2>Our Values</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 col-md-4 col-lg-4 text-center">
                <div class="our_value_content">
                    <figure>
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/thumb_icon.svg" alt="" />
                    </figure>
                    <h3>Passion and speed</h3>
                    <p>We move fast to win and leave no stones unturned.</p>
                </div>
            </div>
            <div class="col-sm-12 col-md-4 col-lg-4 text-center">
                <div class="our_value_content">
                    <figure>
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/check_icon.svg" alt="" />
                    </figure>
                    <h3>Dare to try</h3>
                    <p>Anyone can drive change. We are curious, creative and action biased.</p>
                </div>
            </div>
            <div class="col-sm-12 col-md-4 col-lg-4 text-center">
                <div class="our_value_content">
                    <figure>
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/book_icon.svg" alt="" />
                    </figure>
                    <h3>High standards</h3>
                    <p>Mediocrity is never an option, we strive to be the very best.</p>
                </div>
            </div>
            <div class="col-sm-12 col-md-4 col-lg-4 text-center">
                <div class="our_value_content">
                    <figure>
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/thumb_icon.svg" alt="" />
                    </figure>
                    <h3>Act as an owner</h3>
                    <p>Every employee is a shareholder. We think and act like owners.</p>
                </div>
            </div>
            <div class="col-sm-12 col-md-4 col-lg-4 text-center">
                <div class="our_value_content">
                    <figure>
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/check_icon.svg" alt="" />
                    </figure>
                    <h3>Smart and humble</h3>
                    <p>No ego or big personalities. Just smart people solving hard problems.</p>
                </div>
            </div>
            <div class="col-sm-12 col-md-4 col-lg-4 text-center">
                <div class="our_value_content">
                    <figure>
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/book_icon.svg" alt="" />
                    </figure>
                    <h3>Enjoy the journey</h3>
                    <p>We are here to build a great business and have fun in the process.</p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Our Value Section Ends Here -->

<!-- Two Images Section Starts Here -->
<section class="comman_sec two_images_col_sec">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-4 col-lg-4">
                <div class="two_images_col" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/images/thw_image1.png);"><!-- Column Image Define Here --></div>
            </div>
            <div class="col-sm-12 col-md-8 col-lg-8">
                <div class="two_images_col" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/images/thw_image2.png);"><!-- Column Image Define Here --></div>
            </div>
        </div>
    </div>
</section>
<!-- Two Images Section Ends Here -->

<!-- Trusted By Best Section Starts Here -->
<section class="comman_sec trusted_best_sec">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 text-center">
                <div class="trusted_best_box">
                    <h2>Trusted by the best</h2>
                    <ul class="trusted_best_logo_list row">
                        <li class="col-sm-12 col-md-6 col-lg-6">
                            <figure>
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/gfc.svg" alt="" />
                            </figure>
                        </li>
                        <li class="col-sm-12 col-md-6 col-lg-6">
                            <figure>
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/commerce-innovated.svg" alt="" />
                            </figure>
                        </li>
                    </ul>
                    <p>QuadPay is backed by Global Founders Capital, the venture capital arm of Rocket Internet. GFC operate a $1.7bn venture fund and a $750m debt fund providing QuadPay with the financial capacity and backing required to support merchants of all sizes and stages.</p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Trusted By Best Section Ends Here -->

<!-- Join our mission Section Starts Here -->
<section class="comman_sec our_mission_sec">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 text-center">
                <div class="heading_content">
                    <h2>Join our mission</h2>
                    <p>QuadPay is an incredible career opportunity offering an exciting, fulfilling and rewarding work environment.</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-sm-6 col-md-4 col-lg-4 our_mission_wrap">
                <div class="our_mission_content">
                    <h4>Engineering</h4>
                    <h3>Business Intelligence Analyst</h3>
                    <p>New York, United States</p>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-4 our_mission_wrap">
                <div class="our_mission_content">
                    <h4>Operations</h4>
                    <h3>Corporate Strategy &amp; Financial Analysis</h3>
                    <p>New York, United States</p>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-4 our_mission_wrap">
                <div class="our_mission_content">
                    <h4>Operations</h4>
                    <h3>VP/Director of Finance &amp; Accounting</h3>
                    <p>New York, United States</p>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-4 our_mission_wrap">
                <div class="our_mission_content">
                    <h4>Design</h4>
                    <h3>Product Designer</h3>
                    <p>New York, United States</p>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-4 our_mission_wrap">
                <div class="our_mission_content">
                    <h4>Sales</h4>
                    <h3>Sales Development Representative</h3>
                    <p>New York, United States</p>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-4 our_mission_wrap">
                <div class="our_mission_content">
                    <h4>Design</h4>
                    <h3>Brand Designer</h3>
                    <p>New York, United States</p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Join our mission Section Ends Here -->

<!-- Mid Content Section Starts Here -->
<section class="comman_sec">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 text-center">
                <div class="mid_content">
                    <h2>Get started with QuadPay today</h2>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Mid Content Section Starts Here -->

<?php get_footer(); ?>
