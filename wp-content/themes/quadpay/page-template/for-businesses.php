<?php
/*
* Template Name: For Businesses
*/

get_header(); ?>
	<!-- Mid Content Section Starts Here -->
	<section class="comman_sec mid_content_sec businesses_content-sec">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-12 col-lg-12 text-center">
					<div class="mid_content">
						<h2>Become a Verified Merchant</h2>
						<p>Increase your QuadPay sales up to 100x. Increase your overall sales by 20%.</p> 
                    
                    </div>
				</div>
			</div>
		</div>
	</section>
	<!-- Mid Content Section Starts Here -->
	<!-- Two Images Section Starts Here -->
	<section class="comman_sec two_images_col_sec">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-8 col-lg-8">
					<div class="two_images_col" style="background-image: url(http://172.16.0.87/projects/wp/quadpay/wp-content/themes/quadpay/assets/images/merchant-1.png);">
						<!-- Column Image Define Here -->
					</div>
				</div>
				<div class="col-sm-12 col-md-4 col-lg-4">
					<div class="two_images_col" style="background-image: url(http://172.16.0.87/projects/wp/quadpay/wp-content/themes/quadpay/assets/images/merchant-2.png);">
						<!-- Column Image Define Here -->
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Two Images Section Ends Here -->
	<!--progress baar sec start here-->
	<section class="comman_sec progress_bar_sec">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-12 col-lg-12 text-center">
					<div class="mid_content">
						<h3>Experience incredible growth across all the key metrics that matter to your business</h3>
						<p>and benefit from the growing QuadPay community of shoppers.</p>
					</div>
				</div>
			</div>
			<div class="counter-progress">
				<div class="row">
					<div class="col-sm-2 col-md-3 col-lg-3">
						<div class="progress-bar-content text-center">
							<div class="progress-bar-loader" data-percent="20"> <span class="value">
									<span class="pie-value"></span> <sup>%</sup> </span>
							</div>
							<p><strong>Conversion rate and Topline sales</strong> up more than 20%</p>
						</div>
					</div>
					<div class="col-sm-2 col-md-3 col-lg-3">
						<div class="progress-bar-content text-center">
							<div class="progress-bar-loader" data-percent="60"> <span class="value">
								<span class="pie-value"></span> <sup>%</sup> </span>
							</div>
							<p><strong>Average basket size</strong> up more than 20% and as high as 60%</p>
						</div>
					</div>
					<div class="col-sm-2 col-md-3 col-lg-3">
						<div class="progress-bar-content text-center">
							<div class="progress-bar-loader" data-percent="20"> <span class="value">
								<span class="pie-value"></span> <sup>%</sup> </span>
							</div>
							<p>Up to 20% of <strong>all sales processed through QuadPay</strong></p>
						</div>
					</div>
					<div class="col-sm-2 col-md-3 col-lg-3">
						<div class="progress-bar-content text-center">
							<div class="progress-bar-loader" data-percent="80"> <span class="value">
								<span class="pie-value"></span> <sup>%</sup> </span>
							</div>
							<p>Increased <strong>repeat customer rate</strong> up to 80%</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--progress baar sec end here-->
	<!--pay-quadpay sec start here-->
	<section class="comman_sec pay_quadpay_sec">
		<div class="row">
			<div class="col-sm-12 col-md-6 col-lg-6 col-xl-6">
				<figure class="content_img"> <img src="<?php echo get_template_directory_uri(); ?>/assets/images/quadpay_pay.png" alt="quadpay_pay" /> </figure>
			</div>
			<div class="col-sm-12 col-md-6 col-lg-6 col-xl-6">
				<div class="middle_content">
					<p><strong>Add QuadPay to your website today</strong></p>
					<p>Increase your customers spending power at the checkout with zero interest installment plans. </p>
					<ul class="listing">
						<li>No changes to order management systems.</li>
						<li>No changes to refund processes.</li>
						<li>No changes to finance and reconciliation processes.</li>
					</ul>
				</div>
			</div>
		</div>
	</section>
	<!--pay-quadpay sec end here-->
	<!-- Content Image Section Starts Here -->
	<section class="comman_sec middle_content_img_sec card_sec">
		<div class="container">
			<div class="row align-items-center">
				<div class="col-sm-12 col-md-6 col-lg-6 col-xl-6 order-sm-1 order-md-2 order-lg-2 order-xl-2">
					<figure class="content_img"> <img src="<?php echo get_template_directory_uri(); ?>/assets/images/pay-card.png" alt="pay card" /> </figure>
				</div>
				<div class="col-sm-12 col-md-6 col-lg-6 col-xl-6 order-sm-2 order-md-1 order-lg-1 order-xl-1">
					<div class="middle_content">
						<p><strong>Integrate in a few lines of code</strong></p>
						<p>Whether you process $1m or $1bn in sales, adding QuadPay is straightforward. QuadPay offers an integration method that enables merchants of any size to add QuadPay in a few lines of code.</p>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Content Image Section Ends Here -->
	<!-- Client Logos Section Starts Here -->
	<section class="comman_sec clinet_logo_sec">
		<div class="container">
			<p><strong>SUPPORTED PLATFORMS</strong></p>
			<div class="row align-items-start">
				<div class="col-6 col-md-3">
					<figure> <img src="<?php echo get_template_directory_uri(); ?>/assets/images/client_logo_1.png" alt="client logo" /> </figure>
				</div>
				<div class="col-6 col-md-3">
					<figure> <img src="<?php echo get_template_directory_uri(); ?>/assets/images/client_logo_2.png" alt="client logo" /> </figure>
				</div>
				<div class="col-6 col-md-3">
					<figure> <img src="<?php echo get_template_directory_uri(); ?>/assets/images/client_logo_3.png" alt="client logo" /> </figure>
				</div>
				<div class="col-6 col-md-3">
					<figure> <img src="<?php echo get_template_directory_uri(); ?>/assets/images/client_logo_4.png" alt="client logo" /> </figure>
				</div>
			</div>
			<div class="row justify-content-center">
				<div class="col-3">
					<figure> <img src="<?php echo get_template_directory_uri(); ?>/assets/images/client_logo_5.png" alt="client logo" /> </figure>
				</div>
				<div class="col-3">
					<figure> <img src="<?php echo get_template_directory_uri(); ?>/assets/images/client_logo_6.png" alt="client logo" /> </figure>
				</div>
			</div>
		</div>
	</section>
	<!-- Client Logos Section Ends Here -->
	<!--installment sec start here-->
	<section class="comman_sec installment_sec">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-12 col-lg-12 text-center">
					<div class="mid_content">
						<h3>The only omni-channel installment platform. </h3>
						<p>Enable customers the same interest free payment experience online and in-store with QuadPay.</p>
					</div>
				</div>
			</div>
			<div class="row align-items-center pay-sec">
				<div class="col-sm-12 col-md-6 col-lg-6 col-xl-6">
					<figure class="content_img"> <img src="<?php echo get_template_directory_uri(); ?>/assets/images/pay_system.png" alt="pay_system" /> </figure>
				</div>
				<div class="col-sm-12 col-md-6 col-lg-6 col-xl-6">
					<div class="middle_content">
						<p><strong>Accept QuadPay in-store at 5 or 5,000 locations today. No IT required.</strong></p>
						<p>If you accept Visa, you already accept QuadPay so you don’t need to make any code changes or integrations to any of your Point of Sale or technology systems. </p>
					</div>
				</div>
			</div>
			<div class="row align-items-center location-sec">
				<div class="col-sm-12 col-md-6 col-lg-6 col-xl-6 order-sm-1 order-md-2 order-lg-2 order-xl-2">
					<figure class="content_img"> <img src="<?php echo get_template_directory_uri(); ?>/assets/images/pay_location.png" alt="pay_location" /> </figure>
				</div>
				<div class="col-sm-12 col-md-6 col-lg-6 col-xl-6 order-sm-2 order-md-1 order-lg-1 order-xl-1">
					<div class="middle_content">
						<p><strong>Let us help you promote QuadPay In-Store</strong></p>
						<p>Promote QuadPay in-store, at the point of sale, in your change rooms and on your shelves. You can even change your price tags to display the price divided by 4! </p>
						<p>Our retail promotion kit includes all the tools you need to get started.</p>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--installment sec end here-->
	<!--strory sec start here-->
	<section class="comman_sec story_sec">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-12 col-lg-12 text-center">
					<div class="mid_content">
						<h3>Explore our success stories</h3>
						<p>QuadPay is loved by leading brands everywhere. Make your business accessible to more customers with QuadPay.</p>
					</div>
				</div>
			</div>
			<div class="stories_sec_detail">
				<div class="row align-items-center">
					<div class="col-12 col-sm-4">
						<div class="story_detail text-center">
							<figure> <img src="<?php echo get_template_directory_uri(); ?>/assets/images/story-ugg.png" alt="story-img" /> </figure>
							<p>“We launched QuadPay across Deckers Brands sites including Ugg, Teva, Hoka One One and Sanuk. Within 24 hours of launch QuadPay increased average order value by 30%.”</p> <a href="#" title="LEARN MORE" class="read-more">LEARN MORE</a> </div>
					</div>
					<div class="col-12 col-sm-4">
						<div class="story_detail text-center">
							<figure> <img src="<?php echo get_template_directory_uri(); ?>/assets/images/story-lumee.png" alt="story-img" /> </figure>
							<p>“Within 48 hours of launch, 20% off our website sales were going through QuadPay, most of those being net new incremental customers.“</p> <a href="#" title="LEARN MORE" class="read-more">LEARN MORE</a> </div>
					</div>
					<div class="col-12 col-sm-4">
						<div class="story_detail text-center">
							<figure> <img src="<?php echo get_template_directory_uri(); ?>/assets/images/story-kolo.png" alt="story-img" /> </figure>
							<p>“QuadPay has proven to be a very popular payment option with our customers. For a large number of our customers, is the difference between making or delaying a puchase.“</p> <a href="#" title="LEARN MORE" class="read-more">LEARN MORE</a> </div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--strory sec end here-->
	<!--brand sec start here-->
	<section class="comman_sec brand_sec">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-12 col-lg-12 text-center">
					<div class="mid_content">
						<h3>Hundreds of brands trust and love QuadPay</h3>
						<p>QuadPay is loved by leading brands everywhere. Make your business accessible to more customers with QuadPay.</p>
					</div>
				</div>
			</div>
			<div class="brand_logo text-center">
				<ul>
					<li>
						<figure> <img src="<?php echo get_template_directory_uri(); ?>/assets/images/brand-1.png" alt="brand" /> </figure>
					</li>
					<li>
						<figure> <img src="<?php echo get_template_directory_uri(); ?>/assets/images/brand-2.png" alt="brand" /> </figure>
					</li>
					<li>
						<figure> <img src="<?php echo get_template_directory_uri(); ?>/assets/images/brand-3.png" alt="brand" /> </figure>
					</li>
					<li>
						<figure> <img src="<?php echo get_template_directory_uri(); ?>/assets/images/brand-4.png" alt="brand" /> </figure>
					</li>
					<li>
						<figure> <img src="<?php echo get_template_directory_uri(); ?>/assets/images/brand-5.png" alt="brand" /> </figure>
					</li>
				</ul> <a class="button" href="#" title="EXPLORE OUR DIRECTORY">EXPLORE OUR DIRECTORY</a> </div>
		</div>
	</section>
	<!--brand sec end here-->
	<!--merchant sec start here-->
	<section class="comman_sec merchant_program_sec">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-12 col-lg-12 text-center">
					<div class="mid_content">
						<h3>Become a verified merchant today</h3>
						<p>Join the QuadPay Merchant Program and start promoting QuadPay in-store today and watch your sales run wild!</p> <a class="button" href="#" title="JOIN THE MERCHANT PROGRAM">JOIN THE MERCHANT PROGRAM</a> </div>
				</div>
			</div>
		</div>
	</section>
	<!--merchant sec end here-->
	<?php get_footer(); ?>