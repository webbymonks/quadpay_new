<?php
/*
* Template Name: Join Template
*/

get_header(); ?>
	<!-- Mid Content Section Starts Here -->
	<section class="comman_sec mid_content_sec join-us-sec">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-12 col-lg-12 text-center">
					<div class="mid_content">
						<h2>Become a QuadPay Verified Merchant Today</h2>
						<p>Chances are thousands of customers are already paying with QuadPay on your website and in-store you don’t even know it.</p>
						<figure> <img src="<?php echo get_template_directory_uri(); ?>/assets/images/verified-merchant.png" alt="" /> </figure>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Mid Content Section Starts Here -->
	<!--membership sec start here-->
	<section class="comman_sec membership_sec">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-12 col-lg-12 text-center">
					<div class="heading_content">
						<h2>Membership options</h2>
						<p>Choose an option that best suits your business</p>
					</div>
				</div>
			</div>
			<div class="row membership_detail">
				<!--<div class="col-12 col-sm-9 membership_wrap text-center align-self-center">
					<div class="membership_content">
						<h4>You get paid 1% of transaction volume</h4>
						<p>QuadPay will rebate you 1% of all QuadPay transaction volume. Your customers pay a convenience fee of $1 per installment to use QuadPay.</p> <a href="#" class="button" title="Sign up now">Sign up now</a> </div>
				</div>-->
			<div class="col-12 col-sm-9 membership_wrap text-center align-self-center">
					<div class="membership_content">
						<h4>You offer QuadPay for free to your customers</h4>
						<p>You pay a fee per transaction to QuadPay. Your customers pay nothing.</p> <a href="#" class="button" title="Sign up now">Sign up now</a> </div>
				</div></div>
		</div>
	</section>
	<!--membership section end here-->
	<!-- Our Value Section Starts Here -->
	<section class="comman_sec our_value_sec">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-12 col-lg-12 text-center">
					<h2>MEMBERSHIP BENEFITS</h2> </div>
			</div>
			<div class="row">
				<div class="col-sm-12 col-md-4 col-lg-4 text-center">
					<div class="our_value_content">
						<figure> <img src="<?php echo get_template_directory_uri(); ?>/assets/images/thumb_icon.svg" alt="" /> </figure>
						<h3>Product Detail Page</h3>
						<p>Add the QuadPay widget to your product detail page</p>
					</div>
				</div>
				<div class="col-sm-12 col-md-4 col-lg-4 text-center">
					<div class="our_value_content">
						<figure> <img src="<?php echo get_template_directory_uri(); ?>/assets/images/check_icon.svg" alt="" /> </figure>
						<h3>eCommerce Checkout</h3>
						<p>Add QuadPay to your eCommerce checkout as a payment option</p>
					</div>
				</div>
				<div class="col-sm-12 col-md-4 col-lg-4 text-center">
					<div class="our_value_content">
						<figure> <img src="<?php echo get_template_directory_uri(); ?>/assets/images/book_icon.svg" alt="" /> </figure>
						<h3>Increase Sales</h3>
						<p>Increase online sales by 20%, increase QuadPay sales by up to 100x</p>
					</div>
				</div>
				<div class="col-sm-12 col-md-4 col-lg-4 text-center">
					<div class="our_value_content">
						<figure> <img src="<?php echo get_template_directory_uri(); ?>/assets/images/thumb_icon.svg" alt="" /> </figure>
						<h3>QuadPay Community</h3>
						<p>Have your brand introduced to millions of existing QuadPay customers</p>
					</div>
				</div>
				<div class="col-sm-12 col-md-4 col-lg-4 text-center">
					<div class="our_value_content">
						<figure> <img src="<?php echo get_template_directory_uri(); ?>/assets/images/check_icon.svg" alt="" /> </figure>
						<h3>Activation Kit</h3>
						<p>Have stores? We’ll ship you an in-store activation kit to promote QuadPay in-store</p>
					</div>
				</div>
				<div class="col-sm-12 col-md-4 col-lg-4 text-center">
					<div class="our_value_content">
						<figure> <img src="<?php echo get_template_directory_uri(); ?>/assets/images/book_icon.svg" alt="" /> </figure>
						<h3>Merchant Portal</h3>
						<p>QuadPay Merchant Portal – customer data, order management, reconciliations etc</p>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Our Value Section Ends Here -->
	<!-- Two Images Section Starts Here -->
	<section class="comman_sec two_images_col_sec join_us_col">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-4 col-lg-4 order-sm-1 order-md-2 order-lg-2 order-xl-2">
					<div class="two_images_col" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/images/thw_image3.png);">
						<!-- Column Image Define Here -->
					</div>
				</div>
				<div class="col-sm-12 col-md-8 col-lg-8 order-sm-2 order-md-1 order-lg-1 order-xl-1">
					<div class="two_images_col" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/images/thw_image4.png);">
						<!-- Column Image Define Here -->
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Two Images Section Ends Here -->
	<!-- Client Logos Section Starts Here -->
	<section class="comman_sec clinet_logo_sec">
		<div class="container">
			<p><strong>Quick and easy integration managed by our team, OR BY YOURS</strong></p>
			<div class="row align-items-start">
				<div class="col-6 col-md-3">
					<figure> <img src="<?php echo get_template_directory_uri(); ?>/assets/images/client_logo_1.png" alt="" /> </figure>
				</div>
				<div class="col-6 col-md-3">
					<figure> <img src="<?php echo get_template_directory_uri(); ?>/assets/images/client_logo_2.png" alt="" /> </figure>
				</div>
				<div class="col-6 col-md-3">
					<figure> <img src="<?php echo get_template_directory_uri(); ?>/assets/images/client_logo_3.png" alt="" /> </figure>
				</div>
				<div class="col-6 col-md-3">
					<figure> <img src="<?php echo get_template_directory_uri(); ?>/assets/images/client_logo_4.png" alt="" /> </figure>
				</div>
			</div>
			<div class="row justify-content-center">
				<div class="col-3">
					<figure> <img src="<?php echo get_template_directory_uri(); ?>/assets/images/client_logo_5.png" alt="" /> </figure>
				</div>
				<div class="col-3">
					<figure> <img src="<?php echo get_template_directory_uri(); ?>/assets/images/client_logo_6.png" alt="" /> </figure>
				</div>
			</div>
		</div>
	</section>
	<!-- Client Logos Section Ends Here -->
	<?php get_footer(); ?>