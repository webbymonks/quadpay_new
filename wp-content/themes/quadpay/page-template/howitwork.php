<?php
/*
* Template Name: How it Work
*/

get_header(); ?>
	<!-- Mid Content Section Starts Here -->
	<section class="comman_sec mid_content_sec businesses_content-sec">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-12 col-lg-12 text-center">
					<div class="mid_content">
						<h2>Split any payment into 4 interest free instalments in-store and online</h2>
						<p>It doesn’t matter when or where you shop, as long as you do it with QuadPay, you are in control.</p>
						<figure>
							<img src="<?php echo get_template_directory_uri() ?>/assets/images/verified-merchant.png" alt="" />
						</figure>
                    </div>
				</div>
			</div>
		</div>
	</section>
	<!-- Mid Content Section Starts Here -->
	<!-- Content Image Section Starts Here -->
	<section class="comman_sec middle_content_img_sec card_sec">
		<div class="container">
			<div class="row align-items-center">
				<div class="col-sm-12 col-md-6 col-lg-6 col-xl-6 order-sm-1 order-md-2 order-lg-2 order-xl-2">
					<figure class="content_img"> <img src="<?php echo get_template_directory_uri(); ?>/assets/images/pay-card.png" alt="pay card" /> </figure>
				</div>
				<div class="col-sm-12 col-md-6 col-lg-6 col-xl-6 order-sm-2 order-md-1 order-lg-1 order-xl-1">
					<div class="middle_content">
						<p><strong>Integrate in a few lines of code</strong></p>
						<p>Whether you process $1m or $1bn in sales, adding QuadPay is straightforward. QuadPay offers an integration method that enables merchants of any size to add QuadPay in a few lines of code.</p>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Content Image Section Ends Here -->
	<!--pay-quadpay sec start here-->
	<section class="comman_sec pay_quadpay_sec">
		<div class="row align-items-center">
			<div class="col-sm-12 col-md-6 col-lg-6 col-xl-6">
				<figure class="content_img"> <img src="<?php echo get_template_directory_uri(); ?>/assets/images/quadpay_pay.png" alt="quadpay_pay" /> </figure>
			</div>
			<div class="col-sm-12 col-md-6 col-lg-6 col-xl-6">
				<div class="middle_content">
					<h4>Buy Now. pay later.</h4>
					<p><strong>Add QuadPay to your website today</strong></p>
					<p>Increase your customers spending power at the checkout with zero interest installment plans. </p>
					<ul class="listing">
						<li>No changes to order management systems.</li>
						<li>No changes to refund processes.</li>
						<li>No changes to finance and reconciliation processes.</li>
					</ul>
				</div>
			</div>
		</div>
	</section>
	<!--pay-quadpay sec end here-->

	<!-- Simple Steps Section Starts Here -->
	<section class="simple_steps_sec">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 text-center">
					<div class="heading_content">
						<h2>Buy from anywhere in 3 simple steps</h2>
						<p>Install the app, link your debit or credit card and start shopping. It’s that easy.</p>
					</div>
					<div class="tabing_wrap clearfix">
						<div class="tabs_content">
							<h4>Buy Online and In-store</h4>
							<h2>How it works</h2>
							<div class="nav nav-tabs" id="nav-tab" role="tablist">
								<div class="nav-item active" id="store-website-tab" data-toggle="tab" href="#store-website" role="tab" aria-controls="store-website" aria-selected="true">
									<h3>Search for a store or website</h3>
									<p>Look up the store you would like to transact at or enter a website address.</p>
								</div>
								<div class="nav-item" id="purchase-amount-tab" data-toggle="tab" href="#purchase-amount" role="tab" aria-controls="purchase-amount" aria-selected="false">
									<h3>Enter your purchase amount</h3>
									<p>Enter the amount of your purchase including taxes and shipping.</p>
								</div>
								<div class="nav-item" id="pay-anywhere-tab" data-toggle="tab" href="#pay-anywhere" role="tab" aria-controls="pay-anywhere" aria-selected="false">
									<h3>Swipe, dip or tap to pay anywhere</h3>
									<p>Swipe, dip or tap your QuadPay Visa Card at any store, or use it online to complete your purchase.</p>
								</div>
							</div>
						</div>
						<div class="tab-content" id="nav-tabContent">
							<div class="tab-pane fade show active" id="store-website" role="tabpanel" aria-labelledby="store-website-tab">
								<figure>
									<img src="<?php echo get_template_directory_uri(); ?>/assets/images/phone_img.png" alt="" />
								</figure>
							</div>
							<div class="tab-pane fade" id="purchase-amount" role="tabpanel" aria-labelledby="purchase-amount-tab">
								<figure>
									<img src="<?php echo get_template_directory_uri(); ?>/assets/images/phone_img.png" alt="" />
								</figure>
							</div>
							<div class="tab-pane fade" id="pay-anywhere" role="tabpanel" aria-labelledby="pay-anywhere-tab">
								<figure>
									<img src="<?php echo get_template_directory_uri(); ?>/assets/images/phone_img.png" alt="" />
								</figure>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Simple Steps Section Ends Here -->

	<!-- Our Value Section Starts Here -->
<section class="comman_sec our_value_sec">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 text-center">
                <!-- <h2>Our Values</h2> -->
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 col-md-4 col-lg-4 text-center">
                <div class="our_value_content">
                    <figure>
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/thumb_icon.svg" alt="" />
                    </figure>
                    <h3>Passion and speed</h3>
                    <p>We move fast to win and leave no stones unturned.</p>
                </div>
            </div>
            <div class="col-sm-12 col-md-4 col-lg-4 text-center">
                <div class="our_value_content">
                    <figure>
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/check_icon.svg" alt="" />
                    </figure>
                    <h3>Dare to try</h3>
                    <p>Anyone can drive change. We are curious, creative and action biased.</p>
                </div>
            </div>
            <div class="col-sm-12 col-md-4 col-lg-4 text-center">
                <div class="our_value_content">
                    <figure>
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/book_icon.svg" alt="" />
                    </figure>
                    <h3>High standards</h3>
                    <p>Mediocrity is never an option, we strive to be the very best.</p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Our Value Section Ends Here -->

	<?php get_footer(); ?>