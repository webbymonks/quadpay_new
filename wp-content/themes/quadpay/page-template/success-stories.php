<?php
/*
* Template Name: Success Stories
*/

get_header(); ?>
	<!-- Mid Content Section Starts Here -->
	<section class="comman_sec mid_content_sec story-content-sec">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-12 col-lg-12 text-center">
					<div class="mid_content">
						<h2>Success Stories</h2>
						<p>QuadPay is loved by leading brands everywhere. Make your business accessible to more customers with QuadPay.</p>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Mid Content Section end Here -->
	<!-- Stories Section Starts Here -->
	<section class="story-sec">
		<div class="container">
			<div class="client_story blue-bg">
				<div class="row">
					<div class="col-sm-12 col-md-5 col-lg-5">
						<figure class="content_img" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/images/ugg-story.png)"> </figure>
					</div>
					<div class="col-sm-12 col-md-7 col-lg-7">
						<div class="middle_content">
							<figure><img src="<?php echo get_template_directory_uri(); ?>/assets/images/story-ugg.png" alt="story-img" /></figure>
							<p>We launched QuadPay across Deckers Brands sites including Ugg, Teva, Hoka One One and Sanuk. <strong>Within 24 hours of launch QuadPay increased average order value by 30%.</strong></p>
							<div class="client_name">Anna Doe</div>
							<p> Director of Online Sales — Deckers</p>
						</div>
					</div>
				</div>
			</div>
			<div class="client_story red-bg">
				<div class="row">
					<div class="col-sm-12 col-md-5 col-lg-5 order-sm-1 order-md-2 order-lg-2 order-xl-2">
						<figure class="content_img" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/images/lumee-story.png)"> </figure>
					</div>
					<div class="col-sm-12 col-md-7 col-lg-7 order-sm-2 order-md-1 order-lg-1 order-xl-1">
						<div class="middle_content">
							<figure><img src="<?php echo get_template_directory_uri(); ?>/assets/images/story-lumee.png" alt="story-img" /></figure>
							<p>We launched QuadPay not knowing what to expect but the results were instant. <strong>Within 48 hours of launch, 20% off our website sales were going through QuadPay</strong>, most of those being net new incremental customers.</p>
							<div class="client_name">Angela Shoemake</div>
							<p>CEO — LuMee</p>
						</div>
					</div>
				</div>
			</div>
			<div class="client_story blue-bg">
				<div class="row">
					<div class="col-sm-12 col-md-5 col-lg-5">
						<figure class="content_img" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/images/uncharted-story.png)"> </figure>
					</div>
					<div class="col-sm-12 col-md-7 col-lg-7">
						<div class="middle_content">
							<figure><img src="<?php echo get_template_directory_uri(); ?>/assets/images/ccs-uncharted.png" alt="story-img" /></figure>
							<p>Since we implemented QuadPay, we've received an incredible customer response. <strong>Our customers love the installment sale option and we've seen an increase in conversions.</strong></p>
							<div class="client_name">Christian Schauf</div>
							<p> Co-Founder — Uncharted Supply Co</p>
						</div>
					</div>
				</div>
			</div>
			<div class="client_story red-bg">
				<div class="row">
					<div class="col-sm-12 col-md-5 col-lg-5 order-sm-1 order-md-2 order-lg-2 order-xl-2">
						<figure class="content_img" style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/images/kolo-story.png)"> </figure>
					</div>
					<div class="col-sm-12 col-md-7 col-lg-7 order-sm-2 order-md-1 order-lg-1 order-xl-1">
						<div class="middle_content">
							<figure><img src="<?php echo get_template_directory_uri(); ?>/assets/images/story-kolo.png" alt="story-img" /></figure>
							<p>QuadPay has proven to be a very popular payment option with our customers. <strong>There's no question that for a large number of our customers, is the difference between making or delaying a puchase.</strong></p>
							<div class="client_name">Johannes Quodt</div>
							<p>Co-Founder — KOIO</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Stories Section end Here -->
	<!--merchant sec start here-->
	<section class="comman_sec merchant_program_sec">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-12 col-lg-12 text-center">
					<div class="mid_content">
						<h3>Become a verified merchant today</h3>
						<p>Join the QuadPay Merchant Program and start promoting QuadPay in-store today and watch your sales run wild!</p> <a class="button" href="#" title="JOIN THE MERCHANT PROGRAM">JOIN THE MERCHANT PROGRAM</a> </div>
				</div>
			</div>
		</div>
	</section>
	<!--merchant sec end here-->
	<?php get_footer(); ?>