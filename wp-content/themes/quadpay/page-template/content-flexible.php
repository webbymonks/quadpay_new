<?php
// check if the flexible content field has rows of data
if( get_field('flexible_content') ):

 	// loop through the rows of data
    while ( has_sub_field('flexible_content') ) : ?>

<?php if( get_row_layout() == 'content_section' ): ?>

<!-- Mid Content Section Starts Here -->
<section class="comman_sec mid_content_sec">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 text-center">
                <div class="mid_content">
                    <?php echo get_sub_field('content');?>


                    <?php $button_link = get_sub_field('button_link');?>
                    <?php if($button_link){ ?> <a class="button" href="<?php echo $button_link['url']; ?>"
                        <?php if($button_link[ 'target']) { ?>target="_blank" <?php } ?>
                        title="<?php echo $button_link['title']; ?>"><?php echo $button_link['title']; ?></a>
                    <?php } ?>

                    <?php $image = get_sub_field('image');
                      if($image){ 
                    ?>
                    <figure>
                        <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                    </figure>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Mid Content Section Starts Here -->

<?php elseif( get_row_layout() == 'values_section' ): ?>

<!-- Our Value Section Starts Here -->
<section class="comman_sec our_value_sec">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 text-center">
                <h2><?php echo get_sub_field('values_title'); ?></h2>
            </div>
        </div>

        <?php if( have_rows('add_values') ): ?>
        <div class="row justify-content-center">

            <?php while( have_rows('add_values') ): the_row(); 

		// vars
		$image = get_sub_field('image');
		$title = get_sub_field('title');
        $content = get_sub_field('content');
       
            ?>
            <div class="col-12 col-sm-6 col-md-4 col-lg-4 text-center">
                <div class="our_value_content">
                    <figure>
                        <?php if($image){ ?>
                        <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                    </figure>
                    <?php } ?>
                    <h3><?php echo $title;?></h3>
                    <?php echo $content;?>
                </div>
            </div>
            <?php endwhile; ?>
        </div>
        <?php endif; ?>
    </div>
</section>
<!-- Our Value Section Ends Here -->



<?php elseif( get_row_layout() == 'two_column_image_section' ): ?>



<?php if( have_rows('add_two_column') ): ?>
<!-- Two Images Section Starts Here -->
<section class="comman_sec two_images_col_sec">
    <div class="container">
        <div class="row">

            <?php while( have_rows('add_two_column') ): the_row(); 
            
            $one_image = get_sub_field('one_image');
            $second_image = get_sub_field('second_image');
            $big_image = get_sub_field('big_image');
            ?>
            <?php if( $big_image == 'left' ){ ?>
            <div class="col-sm-12 col-md-8 col-lg-8">
                <div class="two_images_col" <?php if (!empty($second_image)) { ?>
                    style="background-image: url(<?php echo $second_image['url']; ?>);" <?php } ?>></div>
            </div>

            <div class="col-sm-12 col-md-4 col-lg-4">
                <div class="two_images_col" <?php if (!empty($one_image)) { ?>
                    style="background-image: url(<?php echo $one_image['url']; ?>);" <?php } ?>></div>
            </div>
            <?php } else if( $big_image  == 'right' ){ ?>

            <div class="col-sm-12 col-md-4 col-lg-4">
                <div class="two_images_col" <?php if (!empty($one_image)) { ?>
                    style="background-image: url(<?php echo $one_image['url']; ?>);" <?php } ?>></div>
            </div>
            <div class="col-sm-12 col-md-8 col-lg-8">
                <div class="two_images_col" <?php if (!empty($second_image)) { ?>
                    style="background-image: url(<?php echo $second_image['url']; ?>);" <?php } ?>></div>
            </div>
            <?php } ?>
            <?php endwhile;?>
        </div>
    </div>
</section>
<!-- Two Images Section Ends Here -->
<?php endif; ?>
<?php elseif( get_row_layout() == 'trusted_section' ): ?>




<!-- Trusted By Best Section Starts Here -->
<section class="comman_sec trusted_best_sec" id="trustedBest">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 text-center">
                <div class="trusted_best_box">
                    <h2><?php echo get_sub_field('trusted_title');?></h2>

                    <?php if( have_rows('add_logo') ): ?>
                    <ul class="trusted_best_logo_list row">
                        <?php while( have_rows('add_logo') ): the_row(); 
                        $image = get_sub_field('logo');
		                  ?>

                        <?php if($image){ ?>
                        <li class="col-12 col-sm-6 col-md-6 col-lg-6">


                            <figure>

                                <?php $logo_link = get_sub_field('logo_link');
                            ?>
                                <a href="<?php echo $logo_link;?>" target="_blank">
                                    <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />
                                </a>
                            </figure>

                        </li>
                        <?php } ?>
                        <?php endwhile;?>
                    </ul>
                    <?php endif; ?>
                    <?php echo get_sub_field('content');?>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Trusted By Best Section Ends Here -->

<?php elseif( get_row_layout() == 'counter_section' ): ?>

<!--progress baar sec start here-->
<section class="comman_sec progress_bar_sec">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 text-center">
                <div class="mid_content">
                    <?php echo get_sub_field('content');?>
                </div>
            </div>
        </div>

        <?php if( have_rows('add_counter_value') ): ?>
        <div class="counter-progress">
            <div class="row">

                <?php while( have_rows('add_counter_value') ): the_row(); 
                        $value = get_sub_field('value');
                        $content = get_sub_field('content');
		                  ?>

                <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                    <div class="progress-bar-content text-center">
                        <div class="progress-bar-loader" data-percent="<?php echo $value;?>"> <span class="value">
                                <span class="pie-value"></span><sup>%</sup></span>
                        </div>
                        <?php echo $content;?>
                    </div>
                </div>
                <?php endwhile;?>
            </div>
        </div>
        <?php endif; ?>
    </div>
</section>
<!--progress baar sec end here-->

<?php elseif( get_row_layout() == 'quadpay_section' ): ?>


<?php $image = get_sub_field('image');
                  $content = get_sub_field('content');
?>

<!--pay-quadpay sec start here-->
<section class="comman_sec pay_quadpay_sec">
    <div class="row align-items-center">
        <div class="col-sm-12 col-md-6 col-lg-5 col-xl-5 order-sm-2 order-md-1 order-lg-1 order-xl-1">

            <?php if($image){ ?>
            <figure class="content_img"> <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>"
                    title="<?php echo $image['alt']; ?>" /> </figure>
            <?php } ?>
        </div>
        <div class="col-sm-12 col-md-6 col-lg-7 col-xl-7 order-sm-1 order-md-2 order-lg-2 order-xl-2">
            <div class="middle_content">
                <?php echo $content;?>


                <?php $button_link = get_sub_field('button_link');?>
                <?php if($button_link){ ?> <a class="button" href="<?php echo $button_link['url']; ?>"
                    <?php if($button_link[ 'target']) { ?>target="_blank" <?php } ?>
                    title="<?php echo $button_link['title']; ?>"><?php echo $button_link['title']; ?></a>
                <?php } ?>



            </div>
        </div>
    </div>
</section>
<!--pay-quadpay sec end here-->


<?php elseif( get_row_layout() == 'integrate_section' ): ?>
<?php $image = get_sub_field('image');
                  $content = get_sub_field('content');?>

<section class="comman_sec middle_content_img_sec card_sec">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-sm-12 col-md-6 col-lg-6 col-xl-6 order-sm-2 order-md-2 order-lg-2 order-xl-2">

                <?php if($image){ ?>
                <figure class="content_img"> <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>"
                        title="<?php echo $image['alt']; ?>" /> </figure>
                <?php } ?>
            </div>
            <div class="col-sm-12 col-md-6 col-lg-6 col-xl-6 order-sm-1 order-md-1 order-lg-1 order-xl-1">
                <div class="middle_content">
                    <?php echo $content;?>
                </div>
            </div>
        </div>
    </div>
</section>

<?php elseif( get_row_layout() == 'add_logo_section' ): ?>

<!-- Client Logos Section Starts Here -->
<section class="comman_sec clinet_logo_sec">
    <div class="container">
        <p><strong><?php echo get_sub_field('title');?></strong></p>


        <?php if( have_rows('add_logo') ): ?>
        <div class="row align-items-center justify-content-center">

            <?php while( have_rows('add_logo') ): the_row(); 
                        $image = get_sub_field('logo');
		                  ?>
            <?php if($image){ ?>
            <div class="col-6 col-md-3">
                <figure> <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>"
                        title="<?php echo $image['alt']; ?>" /></figure>
            </div>
            <?php } ?>
            <?php endwhile;?>
        </div>
        <?php endif;?>
    </div>
</section>
<!-- Client Logos Section Ends Here -->
<?php elseif( get_row_layout() == 'add_left_right_image_section' ): ?>



<!--installment sec start here-->
<section class="comman_sec installment_sec">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 text-center">
                <div class="mid_content">
                    <?php echo get_sub_field('content');?>
                </div>
            </div>
        </div>

        <?php if( have_rows('add_left_right_content') ): ?>



        <div class="row align-items-center">


            <?php while( have_rows('add_left_right_content') ): the_row(); 
                
                        $image_position = get_sub_field('image_position');
                        $image = get_sub_field('image');
                        $content1 = get_sub_field('content');
		                  ?>




            <?php if( $image_position == 'left' ){ ?>

            <div class="col-sm-12 col-md-12 col-lg-12">
                <div class="row align-items-center pay-sec">
                    <div class="col-sm-12 col-md-6 col-lg-6 col-xl-6">

                        <?php if($image) { ?>
                        <figure class="content_img"> <img src="<?php echo $image['url']; ?>"
                                alt="<?php echo $image['alt']; ?>" title="<?php echo $image['alt']; ?>" /> </figure>
                        <?php } ?>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <div class="middle_content">
                            <?php echo $content1;?>
                        </div>
                    </div>
                </div>
            </div>

            <?php } else if( $image_position  == 'right' ){ ?>
            <div class="col-sm-12 col-md-12 col-lg-12">
                <div class="row align-items-center pay-sec">
                    <div class="col-sm-12 col-md-6 col-lg-6 col-xl-6 order-sm-1 order-md-2 order-lg-2 order-xl-2">
                        <?php if($image) { ?>
                        <figure class="content_img"> <img src="<?php echo $image['url']; ?>"
                                alt="<?php echo $image['alt']; ?>" title="<?php echo $image['alt']; ?>" /> </figure>
                        <?php } ?>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-6 col-xl-6 order-sm-2 order-md-1 order-lg-1 order-xl-1">
                        <div class="middle_content">
                            <?php echo $content1;?>
                        </div>
                    </div>
                </div>
            </div>

            <?php } ?>
            <?php endwhile;?>
        </div>
        <?php endif;?>
    </div>
</section>
<!--installment sec end here-->




<?php elseif( get_row_layout() == 'explore_story_section' ): ?>





<!--strory sec start here-->
<section class="comman_sec story_sec">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 text-center">
                <div class="mid_content">
                    <?php echo get_sub_field('story_content');?>
                </div>
            </div>
        </div>

        <?php if( have_rows('add_stories') ): ?>

        <div class="stories_sec_detail">
            <div class="row align-items-center">


                <?php while( have_rows('add_stories') ): the_row(); 
                
                        
                        $image = get_sub_field('image');
                        $content = get_sub_field('content');
                        $button_link = get_sub_field('button_link');
		                  ?>



                <div class="col-12 col-sm-6 col-md-6 col-lg-4">
                    <div class="story_detail text-center">
                        <?php if($image){ ?>
                        <figure> <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>"
                                title="<?php echo $image['alt']; ?>" /> </figure>
                        <?php } ?>

                        <p><?php echo wp_trim_words((get_sub_field('content')), 30);?></p>
                        <?php //echo $content;?>


                        <?php $button_link = get_sub_field('button_link');?>
                        <?php if($button_link){ ?> <a class="read-more" href="<?php echo $button_link['url']; ?>"
                            <?php if($button_link[ 'target']) { ?>target="_blank" <?php } ?>
                            title="<?php echo $button_link['title']; ?>"><?php echo $button_link['title']; ?></a>

                        <?php } ?>

                    </div>
                </div>
                <?php endwhile;?>
            </div>
        </div>
        <?php endif;?>
    </div>
</section>
<!--strory sec end here-->
<?php elseif( get_row_layout() == 'brand_section' ): ?>
<!--brand sec start here-->
<section class="comman_sec brand_sec">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 text-center">
                <div class="mid_content">
                    <?php echo get_sub_field('brand_content');?>
                </div>
            </div>
        </div>
        <div class="brand_logo text-center">


            <?php if( have_rows('add_brand') ): ?>
            <ul>
                <?php while( have_rows('add_brand') ): the_row(); 
                $logo = get_sub_field('logo');
                $link = get_sub_field('link');
                          ?>

                <?php if($logo) { ?>
                <li>
                    <figure> <?php if($link){ ?><a href="<?php echo $link;?>" target="_blank"> <?php } ?> <img
                                src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt']; ?>"
                                title="<?php echo $logo['alt']; ?>" /><?php if($link) { ?> </a> <?php } ?></figure>
                </li>
                <?php } ?>
                <?php endwhile;?>
            </ul>
            <?php endif;?>

            <?php $explore_link = get_sub_field('explore_link');?>
            <?php if($explore_link){ ?> <a class="button" href="<?php echo $explore_link['url']; ?>"
                <?php if($explore_link[ 'target']) { ?>target="_blank" <?php } ?>
                title="<?php echo $explore_link['title']; ?>"><?php echo $explore_link['title']; ?></a>

            <?php } ?>
        </div>
    </div>
</section>
<!--brand sec end here-->




<?php elseif( get_row_layout() == 'membership_section' ): ?>




<!--membership sec start here-->
<section class="comman_sec membership_sec">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 text-center">
                <div class="heading_content">
                    <?php echo get_sub_field('membership_content');?>
                </div>
            </div>
        </div>
        <?php if( have_rows('add_membership') ): ?>
        <div class="row membership_detail">
            <!--<div class="col-12 col-sm-9 membership_wrap text-center align-self-center">
					<div class="membership_content">
						<h4>You get paid 1% of transaction volume</h4>
						<p>QuadPay will rebate you 1% of all QuadPay transaction volume. Your customers pay a convenience fee of $1 per installment to use QuadPay.</p> <a href="#" class="button" title="Sign up now">Sign up now</a> </div>
				</div>-->

            <?php while( have_rows('add_membership') ): the_row(); 
                $content = get_sub_field('content');
                $button_link = get_sub_field('button_link');
                          ?>



            <div class="col-12 col-sm-12 col-md-10 col-lg-9 membership_wrap text-center align-self-center">
                <div class="membership_content">
                    <?php echo $content;?>


                    <?php $button_link = get_sub_field('button_link');?>
                    <?php if($button_link){ ?> <a class="button" href="<?php echo $button_link['url']; ?>"
                        <?php if($button_link[ 'target']) { ?>target="_blank" <?php } ?>
                        title="<?php echo $button_link['title']; ?>"><?php echo $button_link['title']; ?></a>
                    <?php } ?>
                </div>
            </div>
            <?php endwhile;?>

        </div>
        <?php endif;?>

    </div>
</section>
<!--membership section end here-->



<?php elseif( get_row_layout() == 'story_section' ): ?>

<?php if( have_rows('add_story_section') ): ?>

<section class="story-sec">
    <div class="container">



        <?php while( have_rows('add_story_section') ): the_row(); 

		// vars
		$image = get_sub_field('image');
        $logo = get_sub_field('logo');    
		$content = get_sub_field('content');
		$name = get_sub_field('name');
        $designation = get_sub_field('designation');    
        $image_position = get_sub_field('image_position');    
        ?>

        <?php if( $image_position == 'left' ){ ?>

        <div class="client_story blue-bg">
            <div class="row">
                <div class="col-sm-12 col-md-4 col-lg-4">
                    <figure class="content_img" <?php if (!empty($image)) { ?>
                        style="background-image: url(<?php echo $image['url']; ?>);" <?php } ?>> </figure>
                </div>
                <div class="col-sm-12 col-md-8 col-lg-8">
                    <div class="middle_content">
                        <?php if($logo){ ?>
                        <figure><img src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt']; ?>"
                                title="<?php echo $logo['alt']; ?>" /></figure>
                        <?php } ?>
                        <?php echo $content;?>
                        <div class="client_name"><?php echo $name;?></div>
                        <p><?php echo $designation;?></p>
                    </div>
                </div>
            </div>
        </div>

        <?php } else if( $image_position  == 'right' ){ ?>
        <div class="client_story red-bg">
            <div class="row">
                <div class="col-sm-12 col-md-4 col-lg-4 order-sm-1 order-md-2 order-lg-2 order-xl-2">
                    <figure class="content_img" <?php if (!empty($image)) { ?>
                        style="background-image: url(<?php echo $image['url']; ?>);" <?php } ?>> </figure>
                </div>
                <div class="col-sm-12 col-md-8 col-lg-8 order-sm-2 order-md-1 order-lg-1 order-xl-1">
                    <div class="middle_content">
                        <?php if($logo){ ?>
                        <figure><img src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt']; ?>"
                                title="<?php echo $logo['alt']; ?>" /></figure>
                        <?php } ?>
                        <?php echo $content;?>
                        <div class="client_name"><?php echo $name;?></div>
                        <p><?php echo $designation;?></p>
                    </div>
                </div>
            </div>
        </div>
        <?php } ?>
        <?php endwhile; ?>
        <?php endif; ?>
    </div>
</section>

<?php elseif( get_row_layout() == 'mission_section' ): ?>
<!-- Join our mission Section Starts Here -->
<section class="comman_sec our_mission_sec">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 text-center">
                <div class="heading_content">
                    <?php echo get_sub_field('mission_content');?>
                </div>
            </div>
        </div>
        <?php if( have_rows('add_mission') ): ?>

        <div class="row">

            <?php while( have_rows('add_mission') ): the_row(); 

		// vars
		$name = get_sub_field('name');
        $title = get_sub_field('title'); 
        $location = get_sub_field('location');        
		  
        ?>

            <div class="col-12 col-sm-6 col-md-4 col-lg-4 our_mission_wrap">
                <div class="our_mission_content">
                    <h4><?php echo $name;?></h4>
                    <h3><?php echo $title;?></h3>
                    <p><?php echo $location;?></p>
                </div>
            </div>
            <?php endwhile;?>
        </div>
        <?php endif;?>
    </div>
</section>

<!-- Join our mission Section Ends Here -->


<?php elseif( get_row_layout() == 'form_section' ): ?>

<section class="comman_sec">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 text-center">
                <div class="mid_content">
                    <?php echo get_sub_field('form_content');?>
                </div>
            </div>
        </div>
    </div>
</section>

<?php elseif( get_row_layout() == 'page_content_section' ): ?>



<section class="comman_sec privacy_sec">
    <div class="container">
        <?php echo get_sub_field('content');?>
    </div>
</section>


<?php elseif( get_row_layout() == 'steps_section' ): ?>


<!-- Simple Steps Section Starts Here -->
<section class="comman_sec simple_steps_sec">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 text-center">
                <div class="heading_content">
                    <?php echo get_sub_field('steps_content');?>
                </div>
                <div class="tabing_wrap clearfix">
                    <div class="tabs_content">
                        <?php echo get_sub_field('works_content');?>
                        
                        <?php if( have_rows('add_steps') ): ?>

                        <div class="nav nav-tabs" id="nav-tab" role="tablist">
                            <?php 
                                $i = 0;
                                while( have_rows('add_steps') ): the_row(); 

		// vars
        
		$title = get_sub_field('title');
        $content = get_sub_field('content'); 
        $image = get_sub_field('image');        
		  
        ?>
                            <div class="nav-item <?php if( $i == 0 ){ echo "active"; } ?>" id="<?php echo $i;?>-tab"
                                data-toggle="tab" href="#<?php echo $i;?>" role="tab" aria-controls="<?php echo $i;?>"
                                aria-selected="true">
                                <h3><?php echo $title;?></h3>
                                <?php echo $content;?>
                            </div>
                            <?php $i++;endwhile;?>
                        </div>
                    </div>
                    <?php endif;?>
                    <div class="tab-content" id="nav-tabContent">



                        <?php 
                            $i = 0;
                            while( have_rows('add_steps') ): the_row(); 

		
		$title = get_sub_field('title');
        $content = get_sub_field('content'); 
        $image = get_sub_field('image');        
		  
        ?>
                        <?php if($image) { ?>
                        <div class="tab-pane fade <?php if( $i == 0 ){ echo "show active"; } ?>" id="<?php echo $i;?>"
                            role="tabpanel" aria-labelledby="<?php echo $i;?>-tab">
                            <figure>
                                <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                            </figure>
                        </div>
                        <?php } ?>
                        <?php $i++;endwhile;?>
                    </div>
                </div>

                <div class="tabing_wrap tabing_wrap_mobile clearfix">
                    <div class="tabs_content">
                        <?php echo get_sub_field('works_content');?>
                        <div class="tab-content" id="nav-tabContent">



                        <?php 
                            $i = 11;
                            while( have_rows('add_steps') ): the_row(); 

		
		$title = get_sub_field('title');
        $content = get_sub_field('content'); 
        $image = get_sub_field('image');        
		  
        ?>
                        <?php if($image) { ?>
                        <div class="tab-pane fade <?php if( $i == 11 ){ echo "show active"; } ?>" id="<?php echo $i;?>"
                            role="tabpanel" aria-labelledby="<?php echo $i;?>-tab">
                            <figure>
                                <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                            </figure>
                        </div>
                        <?php } ?>
                        <?php $i++;endwhile;?>
                    </div>
                        <?php if( have_rows('add_steps') ): ?>

                        <div class="nav nav-tabs" id="nav-tab" role="tablist">
                            <?php 
                                $i = 11;
                                while( have_rows('add_steps') ): the_row(); 

		// vars
        
		$title = get_sub_field('title');
        $content = get_sub_field('content'); 
        $image = get_sub_field('image');        
		  
        ?>
                            <div class="nav-item <?php if( $i == 11 ){ echo "active"; } ?>" id="<?php echo $i;?>-tab"
                                data-toggle="tab" href="#<?php echo $i;?>" role="tab" aria-controls="<?php echo $i;?>"
                                aria-selected="true">
                                <h3><?php echo $title;?></h3>
                                <?php echo $content;?>
                            </div>
                            <?php $i++;endwhile;?>
                        </div>
                    </div>
                    <?php endif;?>
                    
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Simple Steps Section Ends Here -->



<?php elseif( get_row_layout() == 'faq_section' ): ?>




<section class="comman_sec frequently_question_sec">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12">
                <h3><?php echo get_sub_field('faq_title');?></h3>

                <?php if( have_rows('add_faq') ): ?>
                <div id="accordion">

                    <?php 
                                $i = 0;
                                while( have_rows('add_faq') ): the_row(); 
                    
                             $title = get_sub_field('title');
                             $content = get_sub_field('content');    

		              ?>

                    <div class="card">
                        <div class="card-header" id="heading<?php echo $i;?>">
                            <div class="collapsed" data-toggle="collapse" data-target="#collapse<?php echo $i;?>"
                                aria-expanded="false" aria-controls="collapse<?php echo $i;?>">
                                <?php echo $title;?>
                            </div>
                        </div>
                        <div id="collapse<?php echo $i;?>" class="collapse" aria-labelledby="heading<?php echo $i;?>"
                            data-parent="#accordion">
                            <div class="card-body">
                                <?php echo $content;?>
                            </div>
                        </div>
                    </div>
                    <?php $i++;endwhile;?>
                </div>
                <?php endif;?>


            </div>
        </div>
    </div>
</section>
<!-- Frequently Asked Question Section Ends Here -->



<?php elseif( get_row_layout() == 'big_content_image_section' ): ?>



<?php if( have_rows('add_content_image') ): ?>



<section class="comman_sec middle_content_img_sec">

    <div class="container">

        <?php while( have_rows('add_content_image') ): the_row(); 
                
                        $image_position = get_sub_field('image_position');
                        $image = get_sub_field('image');
                        $content1 = get_sub_field('content');
                        $button_link = get_sub_field('button_link');
		                  ?>

        <?php if( $image_position == 'left' ){ ?>


        <div class="row align-items-center">
            <div class="col-sm-12 col-md-5 col-lg-5 col-xl-5">


                <?php if($image){ ?>

                <figure class="content_img">
                    <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>"
                        title="<?php echo $image['alt']; ?>" /></figure>
                <?php } ?>
            </div>
            <div class="col-sm-12 col-md-7 col-lg-7 col-xl-7">
                <div class="middle_content">
                    <?php echo $content1;?>
                    <?php $button_link = get_sub_field('button_link');?>
                    <?php if($button_link){ ?> <a class="button" href="<?php echo $button_link['url']; ?>"
                        <?php if($button_link[ 'target']) { ?>target="_blank" <?php } ?>
                        title="<?php echo $button_link['title']; ?>"><?php echo $button_link['title']; ?></a>
                    <?php } ?>
                </div>
            </div>
        </div>



        <?php } else if( $image_position  == 'right' ){ ?>



        <div class="row align-items-center">
            <div class="col-sm-12 col-md-5 col-lg-5 col-xl-5 order-sm-1 order-md-2 order-lg-2 order-xl-2">


                <?php if($image){ ?>

                <figure class="content_img">
                    <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>"
                        title="<?php echo $image['alt']; ?>" /></figure>
                <?php } ?>
            </div>
            <div class="col-sm-12 col-md-7 col-lg-7 col-xl-7 order-sm-2 order-md-1 order-lg-1 order-xl-1">
                <div class="middle_content">
                    <?php echo $content1;?>
                    <?php $button_link = get_sub_field('button_link');?>
                    <?php if($button_link){ ?> <a class="button" href="<?php echo $button_link['url']; ?>"
                        <?php if($button_link[ 'target']) { ?>target="_blank" <?php } ?>
                        title="<?php echo $button_link['title']; ?>"><?php echo $button_link['title']; ?></a>
                    <?php } ?>
                </div>
            </div>
        </div>

        <?php } ?>




        <?php endwhile;?>
    </div>

</section>
<?php endif;?>




<?php elseif( get_row_layout() == 'landing_expansion_section' ): ?>



<section class="comman_sec mid_content_sec expansion_sec">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 text-center">
                <div class="mid_content">
                    <?php $image = get_sub_field('expansion_image');?>
                    <?php if($image) { ?>
                    <p><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>"
                            title="<?php echo $image['alt']; ?>" /></p>
                    <?php } ?>
                    <?php echo get_sub_field('expansion_content');?>


                    <?php 
                            $i = 0;
                            while( have_rows('add_form') ): the_row(); 

                // vars
                $button_name = get_sub_field('button_name');
                
        ?>
                    <a href="#<?php echo $i;?>" title="<?php echo $button_name;?>"
                        class="button <?php if( $i == 1 ){ echo "border_btn"; } ?>" data-toggle="modal"
                        data-target="#<?php echo $i;?>"><?php echo $button_name;?></a>
                    <?php $i++;endwhile; ?>



                </div>
            </div>
        </div>
    </div>
</section>


<?php 
                    $i = 0;
                    while( have_rows('add_form') ): the_row(); 

		// vars
		$form_title = get_sub_field('form_title');
		
?>
<!-- Modal -->
<div class="modal fade" id="<?php echo $i;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <!-- <h3><?php // echo $form_title;?></h3> -->
            </div>
            <div class="modal-body">
                <div class="sign_up_forms">
                    <?php echo do_shortcode(get_sub_field('form_shortcode'));?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $i++;endwhile; ?>



<?php elseif( get_row_layout() == 'sign_up_form_section' ): ?>



<?php $form_content = get_sub_field('form_content');
             $form_shortcode = get_sub_field('form_shortcode');
             $form_position = get_sub_field('form_position');?>



<?php if( $form_position == 'left' ){ ?>


<section class="comman_sec mid_content_sec form_request_section"
    style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/images/form-bg.png)">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-6 col-lg-6 col-xl-6 order-sm-1 order-md-2 order-lg-2 order-xl-2">
                <div class="form-detail">
                    <?php echo $form_content;?>
                    <div class="form_wrap">
                        <div class="sign_up_forms">
                            <?php echo do_shortcode(get_sub_field('form_shortcode'));?>
                        </div>
                    </div>
                    <?php echo get_sub_field('form_after_content');?>

                </div>
            </div>
            <div
                class="col-sm-12 col-md-6 col-lg-6 col-xl-6 order-sm-2 order-md-1 order-lg-1 order-xl-1 hidden-sm-down">
                <div class="form_wrap">
                    <div class="sign_up_forms">
                        <?php echo do_shortcode(get_sub_field('form_shortcode'));?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php } else if( $form_position  == 'right' ){ ?>

<section class="comman_sec mid_content_sec form_request_section form_sec_reverse"
    style="background-image: url(<?php echo get_template_directory_uri(); ?>/assets/images/form-bg-reverse.png)">
    <div class="container">
        <div class="row">
            <div
                class="col-sm-12 col-md-6 col-lg-6 col-xl-6 order-2 order-sm-2 order-md-2 order-lg-2 order-xl-2 hidden-sm-down">
                <div class="form_wrap">
                    <div class="sign_up_forms">
                        <?php echo do_shortcode(get_sub_field('form_shortcode'));?>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-6 col-lg-6 col-xl-6 order-1 order-sm-1 order-md-1 order-lg-1 order-xl-1">
                <div class="form-detail">
                    <?php echo $form_content;?>
                    <div class="form_wrap">
                        <div class="sign_up_forms">
                            <?php echo do_shortcode(get_sub_field('form_shortcode'));?>
                        </div>
                    </div>
                    <?php echo get_sub_field('form_after_content');?>
                </div>
            </div>
        </div>
    </div>
</section>

<?php } ?>



<?php elseif( get_row_layout() == 'get_started_section' ): ?>




<section class="comman_sec account_type_Sec">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 text-center">
                <?php echo get_sub_field('started_content_section');?>
                <div class="row justify-content-md-center">
                    <div class="col-sm-12 col-md-12 col-lg-9">
                        <div class="row">



                            <?php 
                            $i = 0;
                            while( have_rows('add_form_section') ): the_row(); 

		// vars
		$image = get_sub_field('image');
		$button_name = get_sub_field('button_name');
        $form_title = get_sub_field('form_title');
        $form_shortcode = get_sub_field('form_shortcode');
        $choose_form_link = get_sub_field('choose_form_link');
        $button_link = get_sub_field('button_link');

		?>
                            <div class="col-12 col-sm-6 col-md-6 col-lg-6">
                                <div class="account_type_content">
                                    <figure <?php if (!empty($image)) { ?>
                                        style="background-image: url(<?php echo $image['url']; ?>);" <?php } ?>>
                                        <!-- Image Here -->
                                    </figure>



                                    <?php if( $choose_form_link == 'form' ){ ?>

                                    <a href="#<?php echo $i;?>" title="<?php echo $button_name;?>"
                                        class="button <?php if( $i == 1 ){ echo "border_btn"; } ?>" data-toggle="modal"
                                        data-target="#<?php echo $i;?>"><?php echo $button_name;?></a>



                                    <?php } else if( $choose_form_link  == 'button-link' ){ ?>


                                    <a href="<?php echo $button_link;?>" title="<?php echo $button_name;?>"
                                        class="button border_btn"><?php echo $button_name;?></a>
                                    <?php } ?>



                                    <!-- Modal -->
                                    <div class="modal fade" id="<?php echo $i;?>" tabindex="-1" role="dialog"
                                        aria-hidden="true">
                                        <div class="modal-dialog store_popup_content modal-dialog-centered"
                                            role="document">
                                            <div class="modal-content">
                                                <div class="modal-body">
                                                    <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>

                                                    <?php $image = get_field('image','options');
                                                            if($image){ ?>
                                                    <div class="clearfix">
                                                        <figure class="store_popup_img">
                                                            <img src="<?php echo $image['url']; ?>"
                                                                alt="<?php echo $image['alt']; ?>" />
                                                        </figure>
                                                        <?php } ?>

                                                        <div class="banner_content">
                                                            <?php echo get_field('store_popup_content','options');?>
                                                            <?php echo  do_shortcode(get_field('store_form_shortcode','options'));?>
                                                            <ul>
                                                                <?php while (have_rows('add_icons','options')) : the_row(); ?>
                                                                <?php $fa_class = get_sub_field('icon','options');
                                                                $link = get_sub_field('link','options');
                                                                if($fa_class && $link ){ ?>
                                                                <li><a href="<?php echo $link; ?>" target="_blank"
                                                                        title="<?php echo get_sub_field('title'); ?>"><i
                                                                            class="<?php echo $fa_class; ?>"></i><?php echo get_sub_field('title'); ?></a>
                                                                </li>
                                                                <?php } ?>
                                                                <?php endwhile; ?>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Modal -->
                                </div>
                            </div>
                            <?php $i++;endwhile;?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>



<?php elseif( get_row_layout() == 'top_banner_section' ): ?>

<!-- Banner Section Starts Here -->
<section class="comman_sec banner_sec">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-6 col-lg-6 col-xl-6">
                <div class="banner_content">
                    <?php echo get_sub_field('banner_content');?>
                    <?php echo  do_shortcode(get_sub_field('form_shortcode'));?>
                    <ul>
                        <?php while (have_rows('add_icons')) : the_row(); ?>
                        <?php $fa_class = get_sub_field('icon');
                    $link = get_sub_field('link');
                    if($fa_class && $link ){ ?>
                        <li><a href="<?php echo $link; ?>" target="_blank"
                                title="<?php echo get_sub_field('title'); ?>"><i
                                    class="<?php echo $fa_class; ?>"></i><?php echo get_sub_field('title'); ?></a></li>
                        <?php } ?>
                        <?php endwhile; ?>
                    </ul>
                </div>
            </div>
            <div class="col-sm-12 col-md-6 col-lg-6 col-xl-6">

                <?php $image = get_sub_field('image');
                if($image){ ?>

                <figure class="banner_img">
                    <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                </figure>
                <?php } ?>
            </div>
        </div>
    </div>
</section>
<!-- Banner Section Ends Here -->

<?php elseif( get_row_layout() == 'store_section' ): ?>
<section class="comman_sec unlimited_possibiliteis_sec">


    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 text-center">
                <div class="heading_content">
                    <?php echo get_sub_field('store_content');?>
                </div>
            </div>
        </div>
        <?php if( have_rows('add_store') ): ?>


        <div class="row">

            <?php while( have_rows('add_store') ): the_row(); 
        $image = get_sub_field('image');
		$logo = get_sub_field('logo');
		?>
            <div class="col-6 col-sm-6 col-md-4 col-lg-3 col-xl-3">
                <div class="unli_possi_img" <?php if (!empty($image)) { ?>
                    style="background-image: url(<?php echo $image['url']; ?>);" <?php } ?>>
                    <?php if($logo){ ?>
                    <figure>
                        <img src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt']; ?>" />
                    </figure>
                    <?php } ?>
                </div>
            </div>
            <?php endwhile;?>
        </div>


        <?php $button_link = get_sub_field('button_link');?>
        <?php if($button_link){ ?>
        <div class="row button_row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 text-center">
                <a class="button" href="<?php echo $button_link['url']; ?>"
                    <?php if($button_link[ 'target']) { ?>target="_blank" <?php } ?>
                    title="<?php echo $button_link['title']; ?>"><?php echo $button_link['title']; ?></a>
            </div>
        </div>
        <?php } ?>
    </div>
    <?php endif; ?>

</section>


<?php elseif( get_row_layout() == 'signin_account_section' ): ?>



<section class="comman_sec mid_content_sec expansion_sec">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 text-center">
                <div class="mid_content">
                    <?php  echo get_sub_field('account_content');?>


                    <?php 
                $i = 0;
                while( have_rows('add_button') ): the_row(); 


$button_link = get_sub_field('button_link');

?>

                    <?php if($button_link){ ?> <a class="button <?php if( $i == 1 ){ echo "border_btn"; } ?>"
                        href="<?php echo $button_link['url']; ?>" <?php if($button_link[ 'target']) { ?>target="_blank"
                        <?php } ?> title="<?php echo $button_link['title']; ?>"><?php echo $button_link['title']; ?></a>

                    <?php } ?>

                    <?php $i++;endwhile; ?>

                </div>
            </div>
        </div>
    </div>
</section>



<?php elseif( get_row_layout() == 'job_listing_section' ): ?>

<section class="comman_sec our_mission_sec">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 text-center">
                <?php  echo get_sub_field('job_listing_content');?>
            </div>
        </div>
    </div>
</section>



<?php elseif( get_row_layout() == 'membership_benefites__section' ): ?>

<!-- Membership Benefits Section Starts Here -->
<section class="comman_sec membership_benefits_sec">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 text-center">
                <?php echo get_sub_field('membership_benefites_content');?>
            </div>
        </div>

        <?php if( have_rows('add_benefites') ): ?>
        <div class="row justify-content-center">

            <?php while( have_rows('add_benefites') ): the_row(); 

                    // vars
                    $image = get_sub_field('image');
                    $title = get_sub_field('title');
                    $content = get_sub_field('content');
                
                        ?>
            <div class="col-12 col-sm-6 col-md-4 col-lg-4 text-center">
                <div class="our_value_content">
                    <figure>
                        <?php if($image){ ?>
                        <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                    </figure>
                    <?php } ?>
                    <h3><?php echo $title;?></h3>
                    <?php echo $content;?>
                </div>
            </div>
            <?php endwhile; ?>
        </div>
        <?php endif; ?>
        <?php $button_link = get_sub_field('button_link');?>





        <?php if($button_link){ ?>

        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 text-center">

                <a class="button" href="<?php echo $button_link['url']; ?>"
                    <?php if($button_link[ 'target']) { ?>target="_blank" <?php } ?>
                    title="<?php echo $button_link['title']; ?>"><?php echo $button_link['title']; ?></a>
            </div>
        </div>
        <?php } ?>
    </div>
</section>
<!-- Our Value Section Ends Here -->



<?php elseif( get_row_layout() == 'store_directory_section' ): ?>

<!-- Filter Section Starts Here -->
<section class="comman_sec filter_sec">
    <div class="filter_search_sticky">
        <div class="filter_search_sticky_inner">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <div class="filter_search_wrap">
                            <div class="close_btn">
                                <strong>Close</strong>
                                <i class="fas fa-times"><!-- Close Icon --></i>
                            </div>
                            <div class="filter_icon">
                                <strong>Browse</strong>
                                <span>
                                    <!-- Filter Store Menu Icon --></span>
                            </div>
                            <ul class="filter_list">
                                <li class="active"><a href="javascript:void(0);" title="All Stores">All Stores</a></li>

                                <?php  $term = get_queried_object(); ?>




                                <?php $parents_cat_list = get_terms('store_type', array( 'parent' => 0 ) ); // event is taxonomy here ?>



                                <?php foreach( $parents_cat_list as $parent_cats ): ?>

                                 <?php  $category_check = get_field('category_check','store_type_'.$parent_cats->term_id);?>

                                 <?php if($category_check){ ?>

                                <li class="<?php echo $parent_cats->slug;?>"><a href="javascript:void(0);"
                                        title="<?php echo $parent_cats->name;?>"
                                        data-termid="<?php echo $parent_cats->term_id;?>"
                                        data-termslug="<?php echo $parent_cats->slug;?>"><?php echo $parent_cats->name;?></a>
                                </li>

                                <?php } ?>
                                <?php 
                                endforeach;
                                ?>
                            </ul>
                            <div class="search_popup">
                                <div class="blog-sort-search">
                                    <form id="searchtext">
                                        <input type="text" name="search-text" class="search-text" id="search-text"
                                            placeholder="Search" required="required">
                                        <input type="submit" />
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="filter_list_wrap">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12">

                            <ul class="filter_list">
                                <li class="active"><a href="javascript:void(0);" title="All Stores">All Stores</a></li>
                                <?php 
                                $parents_cat_list = get_terms('store_type', array( 'parent' => 0 ) ); // event is taxonomy here
                                foreach( $parents_cat_list as $parent_cats ): ?>
                                <li class="<?php echo $parent_cats->slug;?>"><a href="javascript:void(0);"
                                        title="<?php echo $parent_cats->name;?>"
                                        data-termid="<?php echo $parent_cats->term_id;?>"
                                        data-termslug="<?php echo $parent_cats->slug;?>"><?php echo $parent_cats->name;?></a>
                                </li>
                                <?php 
                                endforeach;
                                ?>
                            </ul>
                        </div>
                    </div>

                </div>
            </div>
        </div>


    </div>
    <div class="blankDivFilter"></div>

    <div class="container project-results_wrap">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 text-center">
                <h4><?php echo get_field('store_heading', 'options'); ?></h4>
            </div>
        </div>
    </div>
    <?php $offset = 24; ?>
    
    
    
<?php $args = array('post_type' => 'store', 'posts_per_page' =>24, 'post_status' => 'publish', 'meta_query' => array(
    'relation' => 'OR',
		array(
			'relation' => 'AND',
                array(
                        'key' => 'featuredimage',
                        'value' => '',
                        'compare' => '!=',
                ),
                array(
                        'key' => 'logourl',
                        'value' => '',
                        'compare' => '!=',
                ),
		),
        array(
                'relation' => 'AND',
                array(
                        'key' => 'logo',
                        'value' => '',
                        'compare' => '!=',
                ),
                array(
                        'key' => 'link',
                        'value' => '',
                        'compare' => '!=',
                ),
		),
    ),
);
        // The Query
        $the_query = new WP_Query( $args );
        //echo "<pre>"; print_r($the_query); exit();
        //print_r($the_query);
        // The Loop
        if($the_query->have_posts()){ ?>
    <div class="container" id="project-results">
        <div class="row">
            <?php
                while ( $the_query->have_posts() ) {
                $cat = array();
                $the_query->the_post();
                $categories = get_the_category();
                foreach($categories as $cd){
                array_push($cat,$cd->name);
                }
                //echo "<pre>";print_r($cat); exit();?>
            <?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>
            <?php //$term_list = wp_get_post_terms($post->ID, 'store_type', array("fields" => "all"));
//print_r($term_list); ?>
           
              
            
                <?php 
                                                    
                     $logourl =  get_post_meta( $GLOBALS['post']->ID, 'logourl', true );
                    //echo  $logourl;
                    //exit();
                        
                     $websiteurl =  get_post_meta( $GLOBALS['post']->ID, 'websiteuel', true );    
                     $featuredimage =  get_post_meta( $GLOBALS['post']->ID, 'featuredimage', true );    
                    //echo get_post_meta( $GLOBALS['post']->ID, 'name', true ); 
                    //echo the_title();   
                    $logo = get_field('logo');
                    
                        ?>  
              
              <?php if(!empty($logourl && $featuredimage ) || !empty($thumb && $logo ) )   { ?>
              
              
              
              
              
              
              
              
           
            <div class="col-6 col-sm-6 col-md-4 col-lg-3 col-xl-3">
                <?php $term_list = wp_get_post_terms($post->ID, 'store_type', array("fields" => "all"));
                    //print_r($term_list) ?>


                <?php foreach ( $term_list as $term ) { ?>
                <?php if($term->name == "Verified Stores") { 
                         $conditional = 'true';
                         break;
                   }else{
                        $conditional = 'false';
                    } } ?>
                <?php if($conditional == 'true'){ ?>


    <?php 
                                                    
                     $logourl =  get_post_meta( $GLOBALS['post']->ID, 'logourl', true );
                    //echo  $logourl;
                    //exit();
                        
                     $websiteurl =  get_post_meta( $GLOBALS['post']->ID, 'websiteuel', true );    
                     $featuredimage =  get_post_meta( $GLOBALS['post']->ID, 'featuredimage', true );    
                    //echo get_post_meta( $GLOBALS['post']->ID, 'name', true ); 
                    //echo the_title();    
                        ?>

                <?php if(!empty($logourl && $featuredimage ))  { ?>


                <a href="<?php echo $websiteurl;?>" target="_blank" title="<?php the_title();?>"
                    class="unli_possi_img verified"
                    <?php if(!empty($featuredimage)){ ?>style="background-image: url(<?php echo $featuredimage; ?>);"
                    <?php } ?>>
                    <span class="verified_text">Verified</span>


                    <?php if($logourl){ ?>
                    <figure>
                        <img src="<?php echo $logourl; ?>" />
                    </figure>
                    <?php } ?>
                </a>

                <?php } else { ?>




                <?php $logo = get_field('logo');?>
                 <?php $link = get_field('link');?>
                <?php if(!empty($logo && $thumb && $link ))  { ?>
               
                <a href="<?php echo $link;?>" target="_blank" title="<?php the_title();?>"
                    class="unli_possi_img verified"
                    <?php if(!empty($thumb)){ ?>style="background-image: url(<?php echo $thumb['0']; ?>);" <?php } ?>>
                    <span class="verified_text">Verified</span>
                </a>
                    <?php } ?>
                    <?php } ?>

                    <?php } else {  ?>
                    
                    
                     <?php $logo = get_field('logo');?>
                    
                    <?php if(!empty($logo && $thumb ))  { ?>
                    
                    <a href="" data-toggle="modal" data-target="#store_popup" title="<?php the_title();?>"
                        class="unli_possi_img"
                        <?php if(!empty($thumb)){ ?>style="background-image: url(<?php echo $thumb['0']; ?>);"
                        <?php } ?>>
                        

                       
                        <?php if($logo){ ?>
                        <figure>
                            <img src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt']; ?>" />
                        </figure>
                        <?php } ?>
                    </a>
                    <?php } ?>

                    <?php if(!empty($logo && $thumb ))  { ?>
                    
                    <a href="<?php echo get_field('mobile_store_link', 'options'); ?>" target="_blank" title="<?php the_title();?>"
                        class="unli_possi_img condifalse unli_possi_img_mobile"
                        <?php if(!empty($thumb)){ ?>style="background-image: url(<?php echo $thumb['0']; ?>);"
                        <?php } ?>>
                        

                       
                        <?php if($logo){ ?>
                        <figure>
                            <img src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt']; ?>" />
                        </figure>
                        <?php } ?>
                    </a>
                    <?php } ?>
                    
                   
                    
                     <?php } ?>
                     <h3><?php the_title();?></h3>
                    <?php //} ?>
            </div>
            
            
            
            
            <?php  } }
                            /* Restore original Post Data */
                            wp_reset_postdata();?>
        </div>
    </div>

    <input type="hidden" name="post_type" value="store" id="post_type">
    <input type="hidden" name="current_termid" value="" id="current_termid">
    <input type="hidden" name="posts_per_page" value="24" id="posts_per_page">
    <input type="hidden" name="total_count" value="<?php echo $the_query->found_posts;?>" id="total_count">
    <input type="hidden" name="offset" value="0" id="offset">
    <input type="hidden" name="filter_type" value="cat_filter" id="filter_type">
    <input type="hidden" value="<?php echo $offset;?>" id="all_offset" name="all_offset">
    <?php } ?>
    <?php if($the_query->found_posts>8){ ?>
    <div class="insight-readmore">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 text-center">
                    <a href="javascript:void(0);" title="Load More" id="loadmore" class="button">
                        <span class="loadmore_btn">Load More</span>
                        <span class="insight-loader">
                            <span class="spinner"></span>
                        </span>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <?php } ?>
</section>
<!-- Filter Section Ends Here -->
<!-- Modal -->
<div class="modal fade" id="store_popup" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog store_popup_content modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <?php $image = get_field('image','options');
                    if($image){ ?>
                <div class="clearfix">
                    <figure class="store_popup_img">
                        <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                    </figure>
                    <?php } ?>

                    <div class="banner_content">
                        <?php echo get_field('store_popup_content','options');?>
                        <?php echo  do_shortcode(get_field('store_form_shortcode','options'));?>
                        <ul>
                            <?php while (have_rows('add_icons','options')) : the_row(); ?>
                            <?php $fa_class = get_sub_field('icon','options');
                        $link = get_sub_field('link','options');
                        if($fa_class && $link ){ ?>
                            <li><a href="<?php echo $link; ?>" target="_blank"
                                    title="<?php echo get_sub_field('title'); ?>"><i
                                        class="<?php echo $fa_class; ?>"></i><?php echo get_sub_field('title'); ?></a>
                            </li>
                            <?php } ?>
                            <?php endwhile; ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php endif;

    endwhile;

else :

    // no layouts found

endif;

?>