// JavaScript Document
var $ = jQuery.noConflict();

var followNav;
var follownavMargin;
function equalhight() {
    $("section").each(function(index, el) {
        if ($(el).find('.our_value_content h3').length > 0) {
            var $height = 0;
            $(el).find('.our_value_content h3').each(function(index2, el2) {
                $(el2).css("height", "auto");
                if (($(el2).innerHeight()) > $height) {
                    $height = $(el2).outerHeight();
                }
            });
            $(el).find('.our_value_content h3').css("height", $height);
        }
    });
}
function equalhight1() {
    $("section").each(function(index, el) {
        if ($(el).find('.our_mission_content h3').length > 0) {
            var $height = 0;
            $(el).find('.our_mission_content h3').each(function(index2, el2) {
                $(el2).css("height", "auto");
                if (($(el2).innerHeight()) > $height) {
                    $height = $(el2).outerHeight();
                }
            });
            $(el).find('.our_mission_content h3').css("height", $height);
        }
    });
}
function equalhight2() {
    $("section").each(function(index, el) {
        if ($(el).find('.whr-items > li .whr-title').length > 0) {
            var $height = 0;
            $(el).find('.whr-items > li .whr-title').each(function(index2, el2) {
                $(el2).css("height", "auto");
                if (($(el2).innerHeight()) > $height) {
                    $height = $(el2).outerHeight();
                }
            });
            $(el).find('.whr-items > li .whr-title').css("height", $height);
        }
    });
}


/* Sticky Header jQuery
================================================*/
function fixedHeader() {
    if ($(window).scrollTop() > follownavMargin) {
        if ($(".blankDiv").length == 0) {
            var blankDiv = $("<div>", {
                "class": "blankDiv"
            });
            blankDiv.outerHeight($("header").outerHeight());
            blankDiv.insertAfter($("#header"));
        }
        $('#header').addClass('fixed');
    } else {
        $('#header').removeClass('fixed');
        $(".blankDiv").remove();
    }
}

/*$.fn.visible = function(partial) {

    var $t            = $(this),
        $w            = $(window),
        viewTop       = $w.scrollTop(),
        viewBottom    = viewTop + $w.height(),
        _top          = $t.offset().top,
        _bottom       = _top + $t.height(),
        compareTop    = partial === true ? _bottom : _top,
        compareBottom = partial === true ? _top : _bottom;

  return ((compareBottom <= viewBottom) && (compareTop >= viewTop));

};*/
$.fn.isOnScreen = function() {
    var win = $(window);
    var viewport = {
        top: win.scrollTop(),
        left: win.scrollLeft()
    };
    viewport.right = viewport.left + win.width();
    viewport.bottom = viewport.top + win.height();
    var bounds = this.offset();
    if (bounds != undefined) {
        bounds.right = bounds.left + this.outerWidth();
        bounds.bottom = bounds.top + this.outerHeight();
        return (!(viewport.right < bounds.left || viewport.left > bounds.right || viewport.bottom < bounds.top || viewport.top > bounds.bottom));
    }
};


function updateActive() {
    if ($(".search-text").val() == '') {
      $(".close_btn").removeClass("focus");
    } else {
      $(".close_btn").addClass("focus");
    }
}


$(document).ready(function(){
    if($('.counter-progress').length > 0){        
        var res_screen = ($('.counter-progress').isOnScreen());        
            if(res_screen == true){
                $('.progress-bar-loader').pieChart({
                    barColor: '#1D73EC',
                    trackColor: '#EAEFFC',
                    lineCap: 'round',
                    lineWidth: 9,
                    size: 180,
                    onStep: function (from, to, percent) {
                        $(this.element).find('.pie-value').text(Math.round(percent));
                    }
                });
            }
    }

    $(".search-text").on("input", updateActive);
    // Also on startup
    updateActive();

    $('.close_btn').click(function(){
        $(".search-text").val('');
        $(this).removeClass('focus');
    });

    $('.filter_icon').click(function(){
        if($(this).is(':visible')){
            $(this).toggleClass('active');
            $('.filter_list_wrap').slideToggle();
            if($('.mobile_top_menu').is(":visible")){
                if($('.filter_icon').hasClass("active")){
                    $('.filter_icon strong').text("Close");
                }else{
                    $('.filter_icon strong').text("Browse");
                }
            }
        }
    });

$('.filter_list li').on('click',function(){
    if($(this).hasClass('active')){
        return false;
    }
    else{
         $('#search-text').val('');
         $(this).addClass("active");
         var get_title = $(this).children("a").attr("title");        
         $(".filter_search_wrap .filter_list li").each(function(){             
             var rel_get_title = $(this).children("a").attr("title"); 
             if(get_title == rel_get_title){                 
                 $(this).siblings().removeClass("active");                               
                 $(this).addClass("active");                 
             }
         });
         $(".filter_list_wrap .filter_list li").each(function(){             
             var rel_get_titles = $(this).children("a").attr("title");                          
             if(get_title == rel_get_titles){                                  
                 $(this).siblings().removeClass("active");
                 $(this).addClass("active");                 
             }
         });
         var term_id = $(this).find('a').attr("data-termid");
         $('#current_termid').val(term_id);
         var post_type = $('#post_type').val();
         $('#offset').val(0);
         var offset = $('#offset').val();
         var posts_per_page = $('#posts_per_page').val();
         $('#filter_type').val('cat_filter');
         var filter_type = $('#filter_type').val();
         ajax_load_filters(term_id, post_type,posts_per_page,offset,filter_type);
         if($('.filter_icon').hasClass('active')) {
            $('.filter_icon').trigger('click');
        }
    }
  });
    /*
* Search via textbox
*/
$('#searchtext').on('submit',function(event){
    event.preventDefault();
    
    $('.filter_list li').siblings().removeClass("active");
   $('#search_type').val('text');
   var search_text = $('#search-text').val();
   var search_type = $('#search_type').val();
   var posts_per_page = $('#posts_per_page').val();
   var offset = $('#offset').val();

   if((offset == null) || (offset==0))
       offset = 8;

   $.ajax({
       url: myAjax_new.ajaxurl,    
       //url: ajax_variable.ajax_url,
       type: 'POST',
       dataType: "json",
       data: {
           'action': 'get_search_post_via_searchbox',
           'search_type': search_type,
           'search_text' : search_text,
           'posts_per_page':posts_per_page,
           'offset' : offset,
       },
       
       beforeSend: function() {
             $('.insight-readmore').show();
            //$('.insight-readmore img').css("display", "block");
        },
        complete: function() {
             $('.insight-readmore').hide();
            //$('.insight-readmore img').css("display", "none");
        },
       
      success: function(r) {
            if (r.response == "success") {
                if(filter_type == 'loadmore'){
                  //alert(r.message)
                    $('#project-results > .row').append(r.message);                          
                }else{
                    $('#project-results > .row').html('');
                    $('#project-results > .row').html(r.message);
                }
                if(r.remaining_posts<=0)
                {
                    $('#loadmore').hide();
                    new_offset = 0;
                }
                else{
                    $('#loadmore').show();
                }   
                $('#total_count').val(r.total_count);
                
                var new_offset = parseInt(offset)+parseInt(posts_per_page);
                $('#all_offset').val(new_offset);
                $('.close').trigger('click');
                
            
            } else if (r.response == "error") {
               // if(filter_type == 'cat_filter'){
                    $('#project-results > .row').html('<div class="no-result">' + r.message + '</div>');
                //}else{
                  //  $('#project-results > .row').append('<div class="no-result">' + r.message + '</div>');
                
            }
        }
       
       
   });
});
var tabChange = function(){
    var tabs = $('.nav-tabs > .nav-item');
    var active = tabs.filter('.active');
    var next = active.next('.nav-item').length? active.next('.nav-item') : tabs.filter(':first-child');
    // Bootsrap tab show, para ativar a tab
    next.tab('show')
}
// Tab Cycle function
var tabCycle = setInterval(tabChange, 60000)
// Tab click event handler
$(function(){
    $('.nav-tabs .nav-item').click(function(e) {
        e.preventDefault();
        // Parar o loop
        clearInterval(tabCycle);
        // mosta o tab clicado, default bootstrap
        $(this).tab('show')
        // Inicia o ciclo outra vez
        setTimeout(function(){
            tabCycle = setInterval(tabChange, 60000)//quando recomeÃ§a assume este timing
        }, 60000);
    });
});
}); // end of ready function

var finish ="";
$(window).on('scroll', function() {
    if($('.counter-progress').length > 0){
        var res_screen = ($('.counter-progress').isOnScreen());        
        if(res_screen == true){
            $('.progress-bar-loader').pieChart({
                barColor: '#1D73EC',
                trackColor: '#EAEFFC',
                lineCap: 'round',
                lineWidth: 9,
                size: 180,
                onStep: function (from, to, percent) {
                    $(this.element).find('.pie-value').text(Math.round(percent));
                }
            });
        }
    }
        fixedHeader();    
        if($(".comman_sec.filter_sec").length > 0){
            if($(window).scrollTop() >= $(".comman_sec.filter_sec").offset().top - $("header").outerHeight()){
                $(".filter_search_sticky").addClass("fixed");
                $(".filter_search_sticky").css("top",$("header").outerHeight());
                $(".blankDivFilter").height($(".filter_search_sticky").outerHeight());
            }
            else{
                $(".filter_search_sticky").removeClass("fixed");
                $(".blankDivFilter").removeAttr("style");

            }
        }
        /* loadmore functionality */
         $("#loadmore").on("click",function(){
            if(!$(".insight-readmore").hasClass("loading_data")){
                   $('#loadmore').show();
                    var term_id = $('#current_termid').val(),
                        post_type = 'store';
                    $('#filter_type').val('loadmore');
                    var filter_type = $('#filter_type').val(),
                        offset = $('#all_offset').val();
                    if((offset == null) || (offset==0))
                        offset = 24;
                    var posts_per_page = $('#posts_per_page').val();
                    ajax_load_filters(term_id, post_type,posts_per_page,offset,filter_type);
                    var new_offset = parseInt(offset)+parseInt(posts_per_page);
                    if(parseInt(new_offset)>=parseInt(total_count)){
                        $('#loadmore').hide();
                        new_offset = 0;
                        $('#all_offset').val(total_count);
                    }
                    $('#offset').val(new_offset);
                }else{
                   e.stopPropagation();
                }
        });     
    });
/* equalhight jQuery
================================================*/
window.onload = function() {
    equalhight();
    equalhight1();
    equalhight2();
    followNav = $('#header').outerHeight();
    follownavMargin = $('.anystore_split_Sec').outerHeight();
    // follownavMargin = parseInt($('#header').css('margin-top'))
    fixedHeader();
}
$(window).resize(function () {
    equalhight();
    equalhight1();
    equalhight2();
    $(".blankDiv").remove();
    followNav = $('#header').outerHeight();
    follownavMargin = $('.anystore_split_Sec').outerHeight();
    // follownavMargin = parseInt($('#header').css('margin-top'))
    fixedHeader();    
    
    if(!$('.filter_icon').is(':visible')){
        $('.filter_list').removeAttr('style');
    }
    if($('.mobile_top_menu').is(":visible")){
        if($('.filter_icon').hasClass("active")){
            $('.filter_icon strong').text("Close");
        }else{
            $('.filter_icon strong').text("Browse");
        }
    }else {
        $('.filter_icon strong').text("Browse");
    }

    if($('.navbar-toggler').is(':visible')) {
        if($('.home').hasClass("modal-open")){
            $('.close').trigger('click');
        }
        if($('.page-id-705').hasClass("modal-open")){
            $('.close').trigger('click');
        }
    }
});
function ajax_load_filters(term_id, post_type,posts_per_page,offset,filter_type){
//alert("fsfsfs");
$.ajax({
        url: myAjax_new.ajaxurl,
        type: 'POST',
        dataType: "json",
        data: {
            'action': 'filter_posts',
            'term_id': term_id,
            'post_type': post_type,
            'posts_per_page': posts_per_page,
            'offset' : offset
        },
        beforeSend: function() {
             $('.insight-readmore').addClass("loading_data");
            //$('.insight-readmore img').css("display", "block");
        },
        complete: function() {
            $('.insight-readmore').removeClass("loading_data");
            //$('.insight-readmore img').css("display", "none");
        },
        success: function(r) {
            if (r.response == "success") {
                if(filter_type == 'loadmore'){
                  //alert(r.message)
                    $('#project-results > .row').append(r.message);                          
                }else{
                    $('#project-results > .row').html('');
                    $('#project-results > .row').html(r.message);
                }
                if(r.remaining_posts<=0)
                {
                    $('#loadmore').hide();
                    new_offset = 0;
                }
                else{
                    $('#loadmore').show();
                }   
                $('#total_count').val(r.total_count);
                
                var new_offset = parseInt(offset)+parseInt(posts_per_page);
                $('#all_offset').val(new_offset);
            
            } else if (r.response == "error") {
                if(filter_type == 'cat_filter'){
                    $('#project-results > .row').html('<div class="no-result">' + r.message + '</div>');
                }else{
                    $('#project-results > .row').append('<div class="no-result">' + r.message + '</div>');
                }
            }
        }
    });
}
/* Bworser jQuery
================================================*/
// [Browser Detection]
(function checkBrowser() {
    var ua;
    if (ua === undefined) {
        ua = window.navigator.userAgent.toLowerCase();
    }

    var matchBrowser = /(edge)\/([\w.]+)/.exec(ua) ||
            /(opr)[\/]([\w.]+)/.exec(ua) ||
            /(chrome)[ \/]([\w.]+)/.exec(ua) ||
            /(iemobile)[\/]([\w.]+)/.exec(ua) ||
            /(version)(applewebkit)[ \/]([\w.]+).*(safari)[ \/]([\w.]+)/.exec(ua) ||
            /(webkit)[ \/]([\w.]+).*(version)[ \/]([\w.]+).*(safari)[ \/]([\w.]+)/.exec(ua) ||
            /(webkit)[ \/]([\w.]+)/.exec(ua) ||
            /(opera)(?:.*version|)[ \/]([\w.]+)/.exec(ua) ||
            /(msie) ([\w.]+)/.exec(ua) ||
            ua.indexOf("trident") >= 0 && /(rv)(?::| )([\w.]+)/.exec(ua) ||
            ua.indexOf("compatible") < 0 && /(mozilla)(?:.*? rv:([\w.]+)|)/.exec(ua) ||
            [];

    var matchPlateform = /(ipad)/.exec(ua) ||
            /(ipod)/.exec(ua) ||
            /(windows phone)/.exec(ua) ||
            /(iphone)/.exec(ua) ||
            /(kindle)/.exec(ua) ||
            /(silk)/.exec(ua) ||
            /(android)/.exec(ua) ||
            /(win)/.exec(ua) ||
            /(mac)/.exec(ua) ||
            /(linux)/.exec(ua) ||
            /(cros)/.exec(ua) ||
            /(playbook)/.exec(ua) ||
            /(bb)/.exec(ua) ||
            /(blackberry)/.exec(ua) ||
            [];

    // Object for matched environment
    var matched = {
        platform: matchPlateform[0] || "",
        browser: matchBrowser[5] || matchBrowser[3] || matchBrowser[1] || "",
        version: matchBrowser[2] || matchBrowser[4] || "0",
        versionNumber: matchBrowser[4] || matchBrowser[2] || "0"
    };

    //console.log(" plateform: " + matched.platform + "\n browser: " + matched.browser + "\n version: " + matched.version + "\n versionNumber: " + matched.versionNumber);

    var plateformClass = matched.platform;
    var browserClass = matched.browser;
    var versionClass = "";

    // If browser is IE than additional class with version
    if (matched.browser === "msie" || matched.browser === "rv") {
        browserClass = "ie";
        versionClass = "ie" + parseInt(matched.versionNumber);
    }

    // Add classes to body
    // alert(plateformClass + " " + browserClass + " " + versionClass);
    $('html').addClass(plateformClass + " " + browserClass + " " + versionClass);
})();