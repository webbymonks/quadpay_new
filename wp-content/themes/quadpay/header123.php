<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="apple-itunes-app" content="app-id=1425045070">
<link rel="profile" href="http://gmpg.org/xfn/11">

<!-- Favicon -->
<?php
$favicon = get_field('favicon', 'option');
if ($favicon) {
    ?>
            <link rel="shortcut icon" type="image/x-icon" href="<?php echo $favicon['url']; ?>">
            <?php } ?>
                <!-- Favicon end -->
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div class="wrapper">

	<section class="anystore_split_Sec">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 text-center">
					<?php echo get_field('top_content','options');?>


		
					<div class="store_link">

					<?php while( have_rows('add_links','options') ): the_row(); 

// vars
$image = get_sub_field('logo','options');
$link = get_sub_field('link','options');

?>
<a target="_blank" href="<?php echo $link;?>">
						<?php if($image) { ?>
						<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />
</a>
						<?php } ?>

						
						<?php endwhile;?>
						
					</div>

				</div>
			</div>
		</div>
	</section>
	<!-- Header Starts Here -->
	<header id="header">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-5 col-lg-4 col-xl-4">
				<?php
					wp_nav_menu(
						array(
							'theme_location' => 'mobile-menu',
							'menu_class'        => 'mobile_top_menu',
						)
					);
				?>
				<?php $header_icon = get_field('logo', 'options');
						if ($header_icon) {?>
						<a class="logo" href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo $header_icon['title']; ?>">
						<img src="<?php echo $header_icon['url']; ?>" alt="<?php echo $header_icon['alt']; ?>"/>
						</a>
				<?php }?>
				<?php $help_link = get_field('help_link','options');?>
				<?php if($help_link){ ?> <a class="help_link" href="<?php echo $help_link['url']; ?>" <?php if($help_link[ 'target']) { ?>target="_blank" <?php } ?> title="<?php echo $help_link['title']; ?>"><?php echo $help_link['title']; ?></a>
												<?php } ?>
				</div>
				<div class="col-sm-12 col-md-7 col-lg-8 col-xl-8">
					<?php get_template_part( 'template-parts/navigation/navigation', 'top' ); ?>
				</div>
			</div>
		</div>
	</header>    
	<!-- Header Ends Here -->
