<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>

	<!-- Footer Starts Here -->
	<footer id="footer">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-9 col-lg-9 col-xl-9 order-2 order-sm-2 order-md-1 order-lg-1 order-xl-1">
				<?php wp_nav_menu(array('theme_location' => 'footer-menu', 'menu_class' => 'foo_menu clearfix', 'menu_id' => '')); ?>
				</div>
				<div class="col-sm-12 col-md-3 col-lg-3 col-xl-3 order-1 order-sm-1 order-md-2 order-lg-2 order-xl-2">
					<div class="copyright_content">
						<?php echo get_field('stay_text','options');?>
						<?php // echo do_shortcode(get_field('footer_form_shortcode','options'));?>

						<form name="iterable_optin" action="//links.iterable.com/lists/addSubscriberForm?emailListId=336359" target="_blank" method="POST" class="email">     <input type="text" name="email" size="22" onfocus="if(this.value===this.defaultValue){this.value='';}" onblur="if(this.value===''){this.value=this.defaultValue;}" value="Enter your email">      <input type="hidden" name="opt_in_source" value="website-footer"     <input type="submit" value="Submit"> </form>


						<ul class="social_icons">
                   <?php while (have_rows('add_social_media', 'options')) : the_row(); ?>
							<?php $fa_class = get_sub_field('icon');
                                $link = get_sub_field('link');
                                 if($fa_class && $link ){ ?>
                            <li><a href="<?php echo $link; ?>" target="_blank" title="<?php echo get_sub_field('title'); ?>" class="fab  <?php echo $fa_class; ?>"></a></li>
								<?php } ?>
									<?php endwhile; ?> 
                    	</ul>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
					<p><?php echo str_replace('#_year_#', date('Y'),get_field('copyright_text', 'options'));?></p>
				</div>
			</div>
		</div>
	</footer>
	<!-- Footer Ends Here -->
</div>
<?php wp_footer(); ?>

</body>
</html>
