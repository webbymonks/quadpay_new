<?php
/**
 * The front page template file
 *
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<!-- Filter Section Starts Here -->
<section class="comman_sec filter_sec">
    <div class="filter_search_sticky">
        <div class="filter_search_sticky_inner">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <div class="filter_search_wrap">
                            <div class="close_btn">
                                <strong>Close</strong>
                                <i class="fas fa-times"><!-- Close Icon --></i>
                            </div>
                            <div class="filter_icon">
                                <strong>Browse</strong>
                                <span>
                                    <!-- Filter Store Menu Icon --></span>
                            </div>
                            <ul class="filter_list">
                                <li class="active"><a href="javascript:void(0);" title="All Stores">All Stores</a></li>

                                <?php  $term = get_queried_object(); ?>




                                <?php $parents_cat_list = get_terms('store_type', array( 'parent' => 0 ) ); // event is taxonomy here ?>



                                <?php foreach( $parents_cat_list as $parent_cats ): ?>

                                 <?php  $category_check = get_field('category_check','store_type_'.$parent_cats->term_id);?>

                                 <?php if($category_check){ ?>

                                <li class="<?php echo $parent_cats->slug;?>"><a href="javascript:void(0);"
                                        title="<?php echo $parent_cats->name;?>"
                                        data-termid="<?php echo $parent_cats->term_id;?>"
                                        data-termslug="<?php echo $parent_cats->slug;?>"><?php echo $parent_cats->name;?></a>
                                </li>

                                <?php } ?>
                                <?php 
                                endforeach;
                                ?>
                            </ul>
                            <div class="search_popup">
                                <div class="blog-sort-search">
                                    <form id="searchtext">
                                        <input type="text" name="search-text" class="search-text" id="search-text"
                                            placeholder="Search" required="required">
                                        <input type="submit" />
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="filter_list_wrap">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12">

                            <ul class="filter_list">
                                <li class="active"><a href="javascript:void(0);" title="All Stores">All Stores</a></li>
                                <?php 
                                $parents_cat_list = get_terms('store_type', array( 'parent' => 0 ) ); // event is taxonomy here
                                foreach( $parents_cat_list as $parent_cats ): ?>
                                <li class="<?php echo $parent_cats->slug;?>"><a href="javascript:void(0);"
                                        title="<?php echo $parent_cats->name;?>"
                                        data-termid="<?php echo $parent_cats->term_id;?>"
                                        data-termslug="<?php echo $parent_cats->slug;?>"><?php echo $parent_cats->name;?></a>
                                </li>
                                <?php 
                                endforeach;
                                ?>
                            </ul>
                        </div>
                    </div>

                </div>
            </div>
        </div>


    </div>
    <div class="blankDivFilter"></div>


    <!-- Banner Section Starts Here -->
    <div class="comman_sec banner_sec">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-6 col-lg-6 col-xl-6 order-2 order-sm-2 order-md-1 order-lg-1 order-xl-1">
                    <div class="banner_content">
                        <?php echo get_field('banner_content');?>
                        <?php echo  do_shortcode(get_field('form_shortcode'));?>
                        <ul>
                            <?php while (have_rows('add_icons')) : the_row(); ?>
                            <?php $fa_class = get_sub_field('icon');
                        $link = get_sub_field('link');
                        if($fa_class && $link ){ ?>
                            <li><a href="<?php echo $link; ?>" target="_blank"
                                    title="<?php echo get_sub_field('title'); ?>"><i
                                        class="<?php echo $fa_class; ?>"></i><?php echo get_sub_field('title'); ?></a>
                            </li>
                            <?php } ?>
                            <?php endwhile; ?>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-6 col-xl-6 order-1 order-sm-1 order-md-2 order-lg-2 order-xl-2">
                    <?php $image = get_field('image');
                    if($image){ ?>
                    <figure class="banner_img">
                        <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                    </figure>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <!-- Banner Section Ends Here -->

    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 text-center">
                <h4><?php echo get_field('store_heading', 'options'); ?></h4>
            </div>
        </div>
    </div>
    <?php $offset = 24; ?>
    
    
    
<?php $args = array('post_type' => 'store', 'posts_per_page' =>24, 'post_status' => 'publish', 'meta_query' => array(
    'relation' => 'OR',
		array(
			'relation' => 'AND',
                array(
                        'key' => 'featuredimage',
                        'value' => '',
                        'compare' => '!=',
                ),
                array(
                        'key' => 'logourl',
                        'value' => '',
                        'compare' => '!=',
                ),
		),
        array(
                'relation' => 'AND',
                array(
                        'key' => 'logo',
                        'value' => '',
                        'compare' => '!=',
                ),
                array(
                        'key' => 'link',
                        'value' => '',
                        'compare' => '!=',
                ),
		),
    ),
);
        // The Query
        $the_query = new WP_Query( $args );
        //echo "<pre>"; print_r($the_query); exit();
        //print_r($the_query);
        // The Loop
        if($the_query->have_posts()){ ?>
    <div class="container" id="project-results">
        <div class="row">
            <?php
                while ( $the_query->have_posts() ) {
                $cat = array();
                $the_query->the_post();
                $categories = get_the_category();
                foreach($categories as $cd){
                array_push($cat,$cd->name);
                }
                //echo "<pre>";print_r($cat); exit();?>
            <?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>
            <?php //$term_list = wp_get_post_terms($post->ID, 'store_type', array("fields" => "all"));
//print_r($term_list); ?>
           
              
            
                <?php 
                                                    
                     $logourl =  get_post_meta( $GLOBALS['post']->ID, 'logourl', true );
                    //echo  $logourl;
                    //exit();
                        
                     $websiteurl =  get_post_meta( $GLOBALS['post']->ID, 'websiteuel', true );    
                     $featuredimage =  get_post_meta( $GLOBALS['post']->ID, 'featuredimage', true );    
                    //echo get_post_meta( $GLOBALS['post']->ID, 'name', true ); 
                    //echo the_title();   
                    $logo = get_field('logo');
                    
                        ?>  
              
              <?php if(!empty($logourl && $featuredimage ) || !empty($thumb && $logo ) )   { ?>
              
              
              
              
              
              
              
              
           
            <div class="col-6 col-sm-6 col-md-4 col-lg-3 col-xl-3">
                <?php $term_list = wp_get_post_terms($post->ID, 'store_type', array("fields" => "all"));
                    //print_r($term_list) ?>


                <?php foreach ( $term_list as $term ) { ?>
                <?php if($term->name == "Verified Stores") { 
                         $conditional = 'true';
                         break;
                   }else{
                        $conditional = 'false';
                    } } ?>
                <?php if($conditional == 'true'){ ?>


    <?php 
                                                    
                     $logourl =  get_post_meta( $GLOBALS['post']->ID, 'logourl', true );
                    //echo  $logourl;
                    //exit();
                        
                     $websiteurl =  get_post_meta( $GLOBALS['post']->ID, 'websiteuel', true );    
                     $featuredimage =  get_post_meta( $GLOBALS['post']->ID, 'featuredimage', true );    
                    //echo get_post_meta( $GLOBALS['post']->ID, 'name', true ); 
                    //echo the_title();    
                        ?>

                <?php if(!empty($logourl && $featuredimage ))  { ?>


                <a href="<?php echo $websiteurl;?>" target="_blank" title="<?php the_title();?>"
                    class="unli_possi_img verified conditrue"
                    <?php if(!empty($featuredimage)){ ?>style="background-image: url(<?php echo $featuredimage; ?>);"
                    <?php } ?>>
                    <span class="verified_text">Verified</span>


                    <?php if($logourl){ ?>
                    <figure>
                        <img src="<?php echo $logourl; ?>" />
                    </figure>
                    <?php } ?>
                </a>

                <?php } else { ?>




                <?php $logo = get_field('logo');?>
                 <?php $link = get_field('link');?>
                <?php if(!empty($logo && $thumb && $link ))  { ?>
               
                <a href="<?php echo $link;?>" target="_blank" title="<?php the_title();?>"
                    class="unli_possi_img verified conditrue"
                    <?php if(!empty($thumb)){ ?>style="background-image: url(<?php echo $thumb['0']; ?>);" <?php } ?>>
                    <span class="verified_text">Verified</span>
                </a>
                    <?php } ?>
                    <?php } ?>

                    <?php } else {  ?>
                    
                    
                     <?php $logo = get_field('logo');?>
                    
                    <?php if(!empty($logo && $thumb ))  { ?>
                    
                    <a href="" data-toggle="modal" data-target="#store_popup" title="<?php the_title();?>"
                        class="unli_possi_img condifalse"
                        <?php if(!empty($thumb)){ ?>style="background-image: url(<?php echo $thumb['0']; ?>);"
                        <?php } ?>>
                        

                       
                        <?php if($logo){ ?>
                        <figure>
                            <img src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt']; ?>" />
                        </figure>
                        <?php } ?>
                    </a>
                    <?php } ?>

                    <?php if(!empty($logo && $thumb ))  { ?>
                    
                    <a href="<?php echo get_field('mobile_store_link', 'options'); ?>" target="_blank" title="<?php the_title();?>"
                        class="unli_possi_img condifalse unli_possi_img_mobile"
                        <?php if(!empty($thumb)){ ?>style="background-image: url(<?php echo $thumb['0']; ?>);"
                        <?php } ?>>
                        

                       
                        <?php if($logo){ ?>
                        <figure>
                            <img src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt']; ?>" />
                        </figure>
                        <?php } ?>
                    </a>
                    <?php } ?>
                    
                   
                    
                     <?php } ?>
                     <h3><?php the_title();?></h3>
                    <?php //} ?>
            </div>
            
            
            
            
            <?php  } }
                            /* Restore original Post Data */
                            wp_reset_postdata();?>
        </div>
    </div>

    <input type="hidden" name="post_type" value="store" id="post_type">
    <input type="hidden" name="current_termid" value="" id="current_termid">
    <input type="hidden" name="posts_per_page" value="24" id="posts_per_page">
    <input type="hidden" name="total_count" value="<?php echo $the_query->found_posts;?>" id="total_count">
    <input type="hidden" name="offset" value="0" id="offset">
    <input type="hidden" name="filter_type" value="cat_filter" id="filter_type">
    <input type="hidden" value="<?php echo $offset;?>" id="all_offset" name="all_offset">
    <?php } ?>
    <?php if($the_query->found_posts>8){ ?>
    <div class="insight-readmore">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 text-center">
                    <a href="javascript:void(0);" title="Load More" id="loadmore" class="button">
                        <span class="loadmore_btn">Load More</span>
                        <span class="insight-loader">
                            <span class="spinner"></span>
                        </span>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <?php } ?>
</section>
<!-- Filter Section Ends Here -->
<!-- Modal -->
<div class="modal fade" id="store_popup" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog store_popup_content modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <?php $image = get_field('image','options');
                    if($image){ ?>
                <div class="clearfix">
                    <figure class="store_popup_img">
                        <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                    </figure>
                    <?php } ?>

                    <div class="banner_content">
                        <?php echo get_field('store_popup_content','options');?>
                        <?php echo  do_shortcode(get_field('store_form_shortcode','options'));?>
                        <ul>
                            <?php while (have_rows('add_icons','options')) : the_row(); ?>
                            <?php $fa_class = get_sub_field('icon','options');
                        $link = get_sub_field('link','options');
                        if($fa_class && $link ){ ?>
                            <li><a href="<?php echo $link; ?>" target="_blank"
                                    title="<?php echo get_sub_field('title'); ?>"><i
                                        class="<?php echo $fa_class; ?>"></i><?php echo get_sub_field('title'); ?></a>
                            </li>
                            <?php } ?>
                            <?php endwhile; ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php store_directory();?>

<?php get_footer();?>